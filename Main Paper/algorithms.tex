\documentclass[main.tex]{subfiles}

\begin{document}

\section{Algorithms}\label{sec.algo}
\subsection{Secret Shared Elgamal Key Encapsulation Protocol}
For this scenario we take a secret sharing scheme \(\mathcal S\), comprised of a set of shareholders \(S = \set*{P_1, \ldots, P_n}\) and an access structure \(\Gamma\), a trusted dealer \(\mathcal D\) and a hard homogeneous space \(\left(\mathcal E, \mathcal G\right)\). Our aim is to provide a protocol so that any third party can encapsulate a 

\begin{bem}
	Algorithm 1 in \cite{PKC:DeFMey20} was called \"{}Threshold Elgamal Decryption\"{}, but actually it is a key exchange protocol, since no specific message is being encapsulated but rather both parties derive a key \(K = H\paren* E = H \paren*{b \ast \left(g^s \ast E_0\right)}\)
\end{bem}

\begin{figure}
	\procedure{Public Parameters}{
		\text{HHS}~ \left(\mathcal E, \mathcal G\right), ~\text{starting element}~E_0 \in \mathcal E, ~\text{distinguished}~ g \in \mathcal G ~\text{with}~ \# g = q,\\
		\text{Hash function}~ \mathcal H : \set*{ 0,1 }^{\ast} \to \set*{0,1}^\lambda\\
		\text{Secret sharing instance}~ \mathcal S~\text{with access structure}~ \Gamma, ~\text{authorised}~ S^\ast= \set*{P_{i_1}, \ldots, P_{i_k}}\in \Gamma
	}

	\procedure[linenumbering, space = auto]{Sign$\paren*{m, S^\ast}$}{
		\left(E_1^0, \ldots, E_\lambda^0\right)\gets \left(E_0, \ldots, E_0\right)\\
		\pcfor j = 1, \ldots, k \\
		%\pcind k \gets k+1\\
		\pcfor l = 1,\ldots, \lambda\\
		\pcif E_l^{j-1} \not\in \mathcal E\\
		P_{i_j} ~\text{outputs}~ \perp~\text{and the algorithm aborts.}\\
		\pcfi\\
		b_{j,l} \sample \Z_q\\
		P_{i_j} ~\text{outputs}~ E_l^j \gets \bracket*{b_{j,l}} E_l^{j-1}.\\
		\pcendfor\\
		\left(c_1, \ldots, c_\lambda\right) \gets \left(\mathcal H\paren*{E_1^k,\ldots, E_\lambda^k,m}\right)\\
		\pcfor j=1,\ldots, k\\
		\pcfor l = 1 ,\ldots, \lambda\\
		\pcif c_{l} = 0\\
		P_{i_j} ~\text{outputs}~ z_{j,l} = b_{j,l}\\
		\pcelse\\
		P_{i_j} ~\text{outputs}~ z_{j,l} = b_{j,l} - \mathsf{input}\paren*{s_{i_j}, S^\ast}\\
		\pcfi\\
		\pcendfor
		\pcfor l = 1 ,\ldots, \lambda\\
		z_l \gets \sum_{j=1}^k z_{j,l}\\
		\pcendfor\\
		\pcendfor\\
		\pcreturn s \gets\left(c_1, \ldots, c_\lambda, z_1, \ldots, z_\lambda\right)
	}
	\label{fig:alg2}
	\caption{Secret Shared HHS Signature}
\end{figure}

\begin{thm}[Simulatability of {\hyperref[fig:sskem]{Algorithm 1}}]
	Let \(\mathcal S = \left(S, \Gamma, G\right)\) be an independently reconstructible secret sharing scheme, that has self contained reconstruction. Also, let \(\left(\mathcal E, \mathcal G\right)\) be an HHS in which the Power-DDHA assumption holds. Furthermore, let \(\mathcal H : \mathcal E \to \set*{0,1}^\lambda\) be a function. If these are fixed as the public parameters of the protocol described in \autoref{fig:sskem}, then the protocol is simulatable.
\label{thm:sim.alg1}\end{thm}

\begin{bem} 
	We restrict ourselves to simulating a successful execution of the decapsultion algorithm  in \autoref{fig:sskem} by the following reasoning: The hard homogeneous space \(\left(\mathcal E, \mathcal G \right)\) is well defined, i.e., for any \(E \in \mathcal E \) and any \(g \in \mathcal G\), we have \(g \ast E \in \mathcal E\). And with the adversary being assumed semi-honest, the abort condition of the decapsulation algorithm is never fulfilled, if the input \(E_b\) is in \(\mathcal E\).
\end{bem}

\begin{proof}[Proof of \autoref{thm:sim.alg1}]
	Let \(S^\ast = \set*{P_{i_1},\ldots,  P_{i_{k'}}}\subset S = \set*{P_1,\ldots, P_k}\) be the subset of shareholders corrupted by an adversary \(\mathcal A\).

	We show, that the output of the simulator \(\mathsf{Sim}_1\) given in \autoref{fig:sim1} is computationally indistinguishable from the transcript of a real execution under the assumption that Power-DDHA is hard in the respective hard homogeneous space \(\left(\mathcal E, \mathcal G\right)\). The simulator's input consists of the shares of the corrupted shareholders \(\set*{s_i}_{P_i \in S^\ast}\), the input into the decapsulation algorithm \(E_b\) and the output \(E^\ast\).
	
	% Sim gets decapsulated value K = H([b \ast g^s \ast] E_0) and the secret shares of the honest party. He has to find a transcript that is consistent
	Similarly to \(\mathsf{Sim}_1\), we first consider the case of one uncorrupted shareholder \(P_{k^\ast}\), and secondly we prove computational indistinguishability for the case of two of more uncorrupted shareholders. 
	We should remark, that -- depending on the secret sharing's access structure \(\Gamma\) -- the case of one uncorrupted shareholder may not even occur.

	\begin{itemize}
	\item 
		For the first case (\(\#S^\ast = \#S-1\)), we denote the uncorrupted shareholder by \(P_{k^\ast}\). By a simple calculation one can see, that the output of \(\mathsf{Sim}_1\) is identical to the transcript of a real execution. 
		\begin{comment}
		The Simulator \(\mathsf{Sim}\) needs to provide the recursive sequence \(E_{\text{dec}}^{1},\ldots, E^{k}_\text{dec}\) to complete the transcript, and he was provided the secret keys \(s_i\), \(i \neq k^\ast\) as input. As described in \autoref{fig:sskem}, \(\mathsf{Sim}\) puts \(E_\text{dec}^0 \gets E_b\) and, for \(j = 1, \ldots, k^\ast-1\), simply computes 
		\[E_\text{dec}^j \gets \bracket*{\mathsf{input}_j \paren*{s_j, S^\ast}}E_\text{dec}^{j-1}.\]
		Now, since \(s_{k^\ast}\) is unknown to \(\simul\), he inversely computes in decreasing order, for \(j = k, \ldots, k^\ast +1\),
		\[ E_\text{dec}^{j-1} \gets \bracket*{\left(\mathsf{input}_j \paren*{s_j, S^\ast}\right)^{-1}} E^j_\text{dec},\]
		thus obtaining 
		\[E_\text{dec}^{k^\ast} = \bracket*{\left(\mathsf{input}_{k^\ast + 1} \paren*{s_{k^\ast + 1}, S^\ast}\right)^{-1}} E_\text{dec}^{k^\ast + 1}.\]
		The simulator \(\simul\) thus outputs the transcript
		\[ \left(E_b, E^1_\text{dec}, \ldots, E^k_\text{dec}, \mathcal K\right) ,\]
		where \(\mathcal K = \mathcal H \paren*{E^k_\text{dec}}\).
		\end{comment}
	\item 
		For the second case, that is \(\#S^\ast < \#S-1\), we denote the uncorrupted shareholders by \(P_{i_1}, \ldots, P_{i_{k'}} = S\setminus S^\ast\), where \(i_1 < i_2 < \ldots <i_{k'}\). Furthermore we denote the transcript of the real execution by \(\left(E_b, E^1, E^2, \ldots, E^k\right)\) and the output of \(\mathsf{Sim}_1\) by \(\left(E_b', {E'}^1, \ldots, {E'}^k\right)\). Suppose, there is a distinguisher \(\mathcal D\), that upon being handed either the real transcript or \(\mathsf{Sim}_1\)'s output can determine with non-negligible probability which choice it received. We will show, that \(\mathcal D\) can break Power-DDHA in \(\left(\mathcal E,\mathcal G\right)\). Let 

			\todo{How to build actual distinguisher against Power-DDHA from the one, that accepts full transcripts?}
	\end{itemize} 
\end{proof}

\begin{bem}
	If \(F = s^a \ast E\), then the transcript \(\left(E_b, E^1, \ldots, E^k\right)\) computed by \(\mathcal D_\text{P-DDHA}\) is identical to a real transcript (of which \"{}the first\"{} share is unknown to \(\mathcal D_\text{P-DDHA}\).

	Hence if \(\mathcal D_\text{trans}\) says that its input from \(\mathcal D_\text{P-DDHA}\) is real, then \(\mathcal D_\text{P-DDHA}\) outputs, that \(F = s^a \ast E\) rather than \(F \sample E\).
	\todo{what todo, if the built transcript is identified as simulated?}
\end{bem}


\begin{defin}[Advantage against \(\mathsf{Exp}_\text{P-DDHA}\)]
	For a algorithm \(\mathcal D\) and a hard homogeneous space \(\left(\mathcal E, \mathcal G\right)\), we define the advantage of \(\mathcal D\) in the  $\mathsf{Exp}^\text{P-DDHA}_{\left(\mathcal E,\mathcal G\right)}$-game as
	\[\advantage{}{\mathsf{Exp}^\text{P-DDHA}_{\left(\mathcal E,\mathcal G\right)}}[\paren*{\mathcal D,\left(\mathcal E, \mathcal G\right)}] = \abs{ \frac 12 - \prob{\mathsf{Exp}^\text{P-DDHA}_{\left(\mathcal E,\mathcal G\right)}\paren*{\mathcal D, \left(\mathcal E,\mathcal G\right)}= \true}}\]
\end{defin}
Similarly, we define the advantage of a distinguisher \(\mathcal D\) in \autoref{fig:gametranscript}.
\begin{defin}[Advantage against \(\mathsf{Exp}_\text{Transcript}\)]
	For an algorithm \(\mathcal D\) and a hard homogeneous space \(\left(\mathcal E,\mathcal G\right)\), we define
	\[ \advantage{}{\mathsf{Exp}^\text{Transcript}_{\left(\mathcal E,\mathcal G\right)}}[\paren*{\mathcal D,\left(\mathcal E, \mathcal G\right)}] = \abs{\frac 12 - \prob{\mathsf{Exp}^\text{Transcript}_{\left(\mathcal E,\mathcal G\right)}\paren*{\mathcal D, \left(\mathcal E, \mathcal G\right)}=\true}}.\]
\end{defin}

In \autoref{fig:distinguisher}, we construct a distinguisher \(\mathcal D_\text{P-DDHA}\) as an adversary for the \(\mathsf{Exp}_{\left(\mathcal E,\mathcal G\right)}^\text{P-DDHA}\) security game in \autoref{fig:gamepddha}, that simulates \(\mathsf{Exp}^\text{Decaps-Transcript}_{\left(\mathcal E, \mathcal D\right)}\) to a distinguisher \(\mathcal D_\text{trans}\) and uses its output. Actually \(\mathcal D_\text{P-DDHA}\) simulates \(\mathsf{Game}_2\) from \autoref{fig:gamehop} to \(\mathcal D_\text{trans}\). We claim, that \(\mathsf{Game}_2\) is computationally indistinguishable from \(\mathsf{Exp}^\text{Decaps-Transcript}_{\left(\mathcal E, \mathcal G\right)}\), decpicted as \(\mathsf{Game}_1\) in \autoref{fig:gamehop}.

\begin{prop}
	The game, that \(\mathcal D_\text{P-DDHA}\) simulates to \(\mathcal D_\text{trans}\) in \autoref{fig:distinguisher}, is computationally indistinguishable from the game in \autoref{fig:gametranscript} under the assumption that \autoref{prob:pddha} is hard.
\label{prop:gamehop}
\end{prop}

\begin{proof}[Proof of \autoref{prop:gamehop}]
	Assume, that they are not. There are two cases to consider: either an honestly generated transcript is handed to \(\mathcal D\) (\(b=0\)), or one that contains randomly selected entries (\(b=1\)). In either case, the entries \(E^2, \ldots, E^{\# S}\) are being generated in an identical manner and are therefore indistinguishable. This reduces the challenge to distinguishing between \(\mathsf{Game}_1\) and \(\mathsf{Game}_2\) in \autoref{fig:gamehop} are in the first two entries of the transcript. Furthermore, the first entry can be considered chosen uniformly at random from \(\mathcal E\) in both games.
	\begin{itemize}
		\item$b=0$: 
			
		\item$b=1$:
	\end{itemize}
\end{proof}

\begin{figure}
	\begin{center}
		\begin{gameproof}[arg=\paren*{\mathcal D, \left(E,F\right)}]
		\gameprocedure[linenumbering, space =auto]{
			b \sample \bin\\
			g \sample \mathcal G ~\text{with}~ \mathsf{ord} g = q\\
			E^0 \sample \mathcal E\pccomment{sample random decapsulation input}\\
			\\
			\text{Let}~ S\in\Gamma\\
			\left(h_1, \ldots, h_{\#S}\right) \sample \bin^{\#S}\setminus\left\{\left(0,\ldots, 0\right)\right\} \\
			s \sample \Z_q\pccomment{generate a random secret}\\ 
			\left(s_1, \ldots, s_{\#S}\right) \gets \mathcal S.\share\paren* s\pccomment{and share it}\\
			\pcfor j = 1 ,\ldots, \#S\\
			\pcif b = 0 \\
			E^j \gets \left[ g^{\mathsf{input}_j \paren*{s_j, S}}\right] E^{j-1}\\
			\pcelseif b=1 \wedge h_j = 1\\
			E^j \sample \mathcal E\\
			\pcfi\\
			b' \gets \mathcal D\paren*{\left(E^0, E^1, \ldots, E^{\#S}\right)}\\
			\pcreturn (b=b')
		}
		\gameprocedure[space =auto]{
			b \sample \bin\\
			g \sample \mathcal G ~\text{with}~ \mathsf{ord} g = q\\
			\gamechange{$E^0 \gets E$}\\
			\gamechange{$E^1 \gets F$}\\
			\text{Let}~ S\in\Gamma\\
			\left(h_1, \ldots, h_{\#S}\right) \sample \bin^{\#S}\setminus\left\{\left(0,\ldots, 0\right)\right\} \\
			s \sample \Z_q\pccomment{generate a random secret}\\ 
			\left(s_1, \ldots, s_{\#S}\right) \gets \mathcal S.\share\paren* s\pccomment{and share it}\\
			\gamechange{$\pcfor j = 2 ,\ldots, \#S$}\\
			\pcif b = 0 \\
			E^j \gets \left[ g^{\mathsf{input}_j \paren*{s_j, S}}\right] E^{j-1}\\
			\pcelseif b=1 \wedge h_j = 1\\
			E^j \sample \mathcal E\\
			\pcfi\\
			b' \gets \mathcal D\paren*{\left(E^0, E^1, \ldots, E^{\#S}\right)}\\
			\pcreturn (b=b')
		}
	\end{gameproof}
	\end{center}
	\caption{\(\mathsf{Exp}^\text{Decaps-Transcript}_{\left(\mathcal E,\mathcal G\right)}\) and the game, that \(\mathcal D_\text{P-DDHA}\) simulates to \(\mathcal D_\text{trans}\)}
	\label{fig:gamehop}
\end{figure}




\end{document}
