\documentclass[main.tex]{subfiles}

\begin{document}
\section{Preliminaries}\label{sec.prelim}

\subsection{Notation}

\[ \left[s\right] E  := g^s \ast E\]

\subsection{Hard Homogeneous Spaces}
In this work we will make use of so-called \"{}hard homogeneous spaces\"{} (HHS). A HHS \(\left(\mathcal E, \mathcal G\right)\) is defined by a set \(\mathcal E\) and a group \(\left(\mathcal G, \odot\right)\) equipped with a transitive group action \( \ast : \mathcal G \times \mathcal E \to \mathcal E\). \(\ast\) has the following properties: 
\begin{itemize}
	\item Compatibility: For any \(g, g'\in \mathcal G\) and any \(E\in \mathcal E\), we have \(g \ast \left( g'\ast E\right) = \left(g\odot g'\right) \ast E\).
	\item Identity:	For any \(E\in\mathcal E\), \(e \ast E = E\) if and only if \(e\in\mathcal G\) is the identity element.
	\item Transitivity: For any \(E,E' \in \mathcal E\), there exists exactly one \(g \in \mathcal G\) such that \(g \ast E = E'\).
\end{itemize}
%\todo{Does \(\mathcal G\) have to be finite? \cite{PKC:DeFMey20} uses \(\#G = q = \# \Z_q\). But can \(\# G = \infty\), e.g. \(G = \Z\)?}

\begin{comment}
\subsubsection{Hard Problems}
\begin{figure}
	\begin{center}
	\procedure[linenumbering, space = auto]{$\mathsf{Exp}^\text{P-DDHA}_{\left(\mathcal E, \mathcal G\right),\adv} \paren*{\lambda}$}{
		b\sample \bin\\
		E \sample \mathcal E\\
		a \sample \set{2, \ldots, \# G-1}\\
		\mathfrak s \sample \mathcal G\\
		\pcif b = 0\\
			F \gets \mathfrak s^a \ast E\\
		\pcelse\\
			F \sample \mathcal E\\
		\pcfi\\
		b' \gets \adv \paren*{\left(a,E,\mathfrak s\ast E, F\right)}\\
		\pcreturn \left( b = b'\right)
	}
	\end{center}
	\caption{The Power-DDHA game}
	\label{fig:gamepddha}
\end{figure}
\end{comment}

In a hard homogeneous space \(\left(\mathcal E, \mathcal G\right)\) we consider the following computational problems:

\begin{comment}
\begin{problem}[Power-DDHA]
	For an \(E \in \mathcal E\), \(\mathfrak s \in \mathcal G\) and \(a \in \set{2, \ldots, \#G-1}\) chosen uniformly at random, the tuple \(\left(a, E, s\ast E, F\right)\) defines an instance of the \emph{Power-DDHA problem}, where either \(F = \mathfrak s ^a \ast E\) or \(F\) was drawn uniformly at random from \(\mathcal E\). The challenge is for an adversary \(\adv\) to determine whether \(F = \mathfrak s^a \ast E\) or \(F\) was drawn from \(\mathcal E\) uniformly at random.
\label{prob:pddha}\end{problem}
A sketch of the problem can be found in \autoref{fig:gamepddha}.
\begin{defin}
	For an instance \(\left(a,E,s\ast E, F\right)\) of the Power-DDHA problem, we define the adversary \(\adv\)'s advantage in the \(\mathsf{Exp}^\text{P-DDHA}_{\left(\mathcal E, \mathcal G\right),\adv} \) game as
	\[\advantage{\text{P-DDHA}}{\left(\mathcal E, \mathcal G\right), \adv}[\paren* \lambda] = \abs{ \frac 12 - \prob{\mathsf{Exp}^\text{P-DDHA}_{\left(\mathcal E, \mathcal G\right),\adv}  = \true}}. \]
\end{defin}
\end{comment}

These problems are assumed to be easily computable in a HHS \(\left(\mathcal E, \mathcal G\right)\), i.e., there exist polynomial time algorithms  that solve them:
\begin{itemize}
	\item Group operations on \(\mathcal G\) (membership, inverting elements, evaluating \(\odot\)).
	\item Sampling elements of \(\mathcal E\) and \(\mathcal G\).
	\item Testing the membership of \(\mathcal E\).
	\item Computing the transitive group action: given \(g\in \mathcal G\) and \(E\in\mathcal E\) as input, compute \(g \ast E\).
\end{itemize}

The following problems are assumed to be hard in a HHS.

\begin{problem}[Group Action Inverse Problem (GAIP)]
	Given two elements \(E,E'\) of \(\mathcal E\) as input, the challenge is to provide \(g \in \mathcal G\) with \(E'= g \ast E\).

	Due to the transitivity property of hard homogeneous spaces, such a \(g\) always exists, thus any instance of the GAIP always has a solution.
\label{prob:gaip}\end{problem}

\begin{problem}[Parallelisation Problem]
	An instance of the \emph{Parallelisation Problem} is defined by a triple \(\left(E, E', F\right)\in \mathcal E^3\) with \(E' = g\ast E\). The challenge is to provide \(F'\) with \(F' = g\ast F\).
\end{problem}

The decisional version of the parallelisation problem is its intuitive continuation:
\begin{problem}[Decisional Parallelisation Problem]
	Given two tuples \(\left(E,E'\right)\) and \(\left(F,F'\right)\) as input, where \(E' = g\ast E\), the \emph{Decisional Parallelisation Problem} is to determine whether \(F' = g\ast F\) or \(F \sample \mathcal E\).
\end{problem}

\begin{bem}
	It is obvious that the decisional parallelisation problem reduces to the parallelisation problem, which in turn reduces to the group action inverse problem.
\end{bem}

\begin{comment}
\begin{problem}[DDH in HHS]
	An instance of \emph{DDH} in a hard homogeneous space \(\left(\mathcal E, \mathcal G\right)\) is defined by a tuple \(\left(a, E, F\right)\), where \(1< a < \#G\) and \(E, F \in \mathcal E\). An adversary's challenge is to determine whether \(F = s^a \ast E\) for some \(s\in \mathcal G\) or \(F\) was drawn from \(\mathcal E\) uniformly at random.
\label{prob:ddh}\end{problem}

\begin{bem}
	It is evident, that an instance of Power-DDHA \(\left(a, E ,\mathfrak s^a \ast E\right)\) can easily be reduced to distuinguishing tuples  \(\left(E,F\right)\), where \(F \sample \mathcal E\) is either sampled randomly from \(\mathcal E\) or \(F = \mathfrak s^a \ast E\). %Given an efficient distinguisher \(\mathcal D\) for tuples \(\left(E,F\right)\) and \(\left(E,s^a\ast F\right)\), we construct an adversary \(\mathcal D'\) against Power-DDHA by simply having \(\mathcal D'\) pass on the tuple \(\left(E,F\right)\) to \(\mathcal D\) and outputting whatever \(\mathcal D'\) outputs.
	
\end{bem}
\end{comment}


\subsection{Secret Sharing Schemes}
Secret sharing schemes are used to distribute a secret \(s\) from a secret space \(G\) among a set of shareholders \(S = \set{P_1, \ldots, P_n}\). A subset \(S^\ast \subset S\) that is able to reconstruct a secret from their respective shares is called \emph{authorised}. The set of all authorised subsets of shareholders is called the \emph{access structure} \(\Gamma\). An instance \(\mathcal S\) of a secret sharing scheme is thusly defined by a triple \(\mathcal S = \left(S, \Gamma, G\right)\).

Any secret sharing instance \(\mathcal S\) provides two algorithms: \(\share\) and \( \rec\). For a given secret \(s\), a dealer executes \(\share\paren* s\) to generate shares \(s_1,\ldots, s_k\). A share \(s_i\) is assigned to a shareholder \(P_{\phi\paren* i}\) via a surjective map \(\phi : \set{1,\ldots, k} \to \set{1, \ldots,n}\) induced by \(\Gamma\). An authorised set of shareholders \(S^\ast\in \Gamma\) executes \(\rec\paren*{\set{s_i}_{P_{\phi\paren* i} \in S^\ast}}\) on their respective shares to retrieve a previously shared secret.

\begin{comment}
To execute \(\rec\), each participating shareholder \(P_i\in S^\ast\) computes his reconstruction input, which we denote by \(\mathsf{input}_i\paren*{s_i}\), and provides it for the reconstruction procedure. In short, we have
\[ s = \rec \paren*{\left\{\mathsf{input}_i\paren*{s_i}\right\}_{P_i \in S^\ast}}.\]
The input \(\mathsf{input}_i\paren*{s_i}\) of party \(P_i\) depends on the share \(s_i\), and may depend on the make of the set authorised set \(S^\ast\) reconstructing a secret \(s\), the other parties' inputs or the access structure.

\begin{exm}[Additive Secret Sharing]
If we consider the case of simple additive secret sharing as an example, that is
\[ s = \sum_{i=1}^{\#S} s_i ,\]
where, for \(i=1, \ldots, \#S-1\), the shares \(s_i\) are picked randomly and \(s_{\#S} = s - \sum_{i=1}^{\#S-1}\), we have
\[\mathsf{input}_i\paren*{s_i} = s_i.\]
\end{exm}
\end{comment}

\begin{exm}[Shamir's secret sharing]
	Shamir's famous secret sharing scheme is defined by \(S=\set{P_1, \ldots, P_n}\), the access structure \(\Gamma = \set{S^\ast \subset S \colon \# S^\ast \geq t}\) for some fixed \(1\leq t \leq n\) and the secret space \(G = \Z_p := \Z \bmod p\)  for some prime \(p > n\). To share a secret \(s \in \Z_p\), a random polynomial \(f \in \Z_p \left[ X \right]\) with constant term \(s\) of degree \(t-1\) is sampled and the shares are defined by
	\[ s_i = f\paren* i\]
	for \( i = 1, \ldots, n\). The assigning function \(\phi\) is simply the identity function. Reconstruction is achieved by summing up \(L_{i,S^\ast} s_i\) of all shareholders \(P_i\) in an authorised set \(S^\ast\), i.e.,
	\[ s = \sum_{P_i \in S^\ast} L_{i,S^\ast} s_i, \]
	where \(L_{i,S^\ast}\) denotes the Lagrange interpolation coefficients.
\end{exm}

%In this work, however, we assume, that \(\mathsf{input}_i\) only depends on \(P_i\)'s share and \(S^\ast\) (which in both examples above is satisfied). Furthermore, we assume the secret sharing schemes to agree with the group action of the group \(G\), form which the secret are picked. That is, \(\mathcal S\) provides an algorithm \(\add\), that has the shareholders combine their shares of two shared secret \(s, s'\in G\) and obtain a share of \(s \ast s'\). In short, for an authorised set of shareholders \(S^\ast \in \Gamma\), we have
%\[ \rec\paren*{\add\paren*{\set*{s_i}_{P_i \in S^\ast},\set*{s'_i}_{P_i \in S^\ast}}} = \rec\paren*{\set*{s_i}_{P_i \in S^\ast}} \ast \rec\paren*{\set*{s'_i}_{P_i \in S^\ast}}, \]
%where \(\left(s_1, \ldots, s_{\#S}\right) \gets \share\paren* s\) and \(\left(s'_1, \ldots, s'_{\#S}\right) \gets \share \paren*{s'}\).

\begin{defin}[Independent Reconstruction]
	We say a secret sharing scheme \(\mathcal S = \left(S, \Gamma, G\right)\) is \textbf{independently reconstructible}, if, for any \(S^\ast\in\Gamma\) and a shareholder \(P_i\in S^\ast\), \(P_i\)'s input to a reconstruction is independent of the other shareholders' inputs.
\end{defin}
Shamir's secret sharing is independently reconstructible.

\begin{comment}
\begin{defin}[Self contained Reconstruction]
	We say a secret sharing scheme \(\mathcal S = \left(S, \Gamma, G\right)\) has \textbf{self contained reconstruction}, if, for any \(S^\ast \in \Gamma\), \(\rec\) can be represented by iterated applications of \(\ast\), the group action of \(G\), on the set of inputs \(\left\{\mathsf{input_i}\right\}_{P_i \in S^\ast}\).
\end{defin}

\begin{prop}
	independent reconstruction \(\Rightarrow\) parallel \(\Rightarrow\) sequential (minor adjustments to algorithm, if any)
\end{prop}

\end{comment}

\subsection{Piecewise Verifiable Proofs}
The concept of piecewise verifiable proofs (PVP)  in the context of hard homogeneous spaces was first introduced by Beullens et al. \cite{EPRINT:BDPV20}. It is a compact non-interactive zero-knowledge proof of a statement
\[ \left(\left(R,R'\right), s_1, \ldots, s_n\right),\]
where we have \(R' = \left[s_0\right] R \in \mathcal E\) and \(s_i = f\paren* i\) for a polynomial \(f \in \Z_p \left[X\right]\) and \(i = 0, \ldots, n\).

The proving and verifying algorithms can be found in \autoref{fig.pvpp} and \autoref{fig.pvpv}, respectively. Combined they state a complete, sound and zero-knowledge non-interactive PVP. For the proofs of security we refer to \cite{EPRINT:BDPV20}, Appendix.

\subsection{Zero-Knowledge Proofs in Hard Homogeneous Spaces}
In this work we do not only make use of PVPs to prove knowledge of a sharing polynomial, we also need a zero-knowledge proof of knowledge of an element \(s\in\mathcal G\) in a HHS \(\left(\mathcal E,\mathcal G\right)\) that links a set of tuples \(\set{\left(E_i,E_i'\right)}_{i = 1 ,\ldots, m}\) in that 
\[\forall i = 1, \ldots, m \colon E_i' = \left[s\right] E_i.\]
The proving algorithm is presented in \autoref{fig.zkp} and the verifying algorithm can be found in \autoref{fig.zkv}. We again refer to \cite{EPRINT:BDPV20} for the proof the presented algorithms being zero-knowledge and sound with regards to a security parameter \(\lambda\).

\subsection{The Adversary}
	In this work we consider a static and active adversary. That is at the beginning of the protocol, the adversary corrupts a set of shareholders. It sees their inputs and controls their outputs, which do not have to adhere to the protocol. This set may not be changed throughout the execution of the protocol.

	The adversary's aim is two-fold. On the one hand it wants to obtain information on the uncorrupted parties' inputs, on the other hand it wants to interfere with the execution of our protocol withouth being detected.
	
\subsection{Communication channels}
Our protocol includes two kinds of parties. A dealer who samples and shares a private key and publishes to according public key. The second kind are the shareholders, among whom the private key is being shared and who execute the distributed decapsulation algorithm in our key exchange mechanism.

For the communication between the dealer and the  shareholders we assume a secure private channels. This means that messages sent through this channel cannot be tampered with without detection or eavesdropped upon. For the communication between the shareholders, however, a simple public channel is sufficient, since all their messages are being bdoardast.


\end{document}
