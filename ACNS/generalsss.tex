\section{Generalising the Secret Sharing Schemes}\label{sec.generalsss}

We constructed the protocols above in the context of Shamir's secret sharing protocol \cite{Shamir:1979:HSS}. The key exchange mechanism in \secref{sec.kem} as well as the signature scheme in \secref{sec.signatures} can be extended to more general secret sharing schemes. In the following, we characterise the requirements that a secret sharing scheme has to meet in order to successfully implement the key exchange mechanism and the signature scheme.
%\subsection{Compatibility requirements}

\ifsubsection
\subsection{Compatibility Requirements}
\else
\noindent\textbf{\\Compatibility Requirements.}
\fi
\begin{definition}[Independent Reconstruction]
	We say a secret sharing instance \(\mathcal S = \left(S, \Gamma, G\right)\) is \emph{independently reconstructible}, if, for any shared secret \(s \in G\), any \(S'\in\Gamma\) and any shareholder \(P_i\in S'\), \(P_i\)'s input to reconstructing \(s\) is independent of the share of each other engaged shareholder \(P_j\in S'\).
\end{definition}
A secret sharing scheme compatible with our key exchange mechanism and signature scheme has to be independently reconstructible, since each shareholder's input into the threshold group action is hidden from every other party by virtue of the GAIP.
% This criterion is clearly met by Shamir's secret sharing.
\begin{definition}[Self-contained reconstruction]
	An instance \(\mathcal S =\left(S,\Gamma,G\right)\) of a secret sharing scheme is called \emph{self-contained}, if, for any authorised set \(S'\), the input of any shareholder \(P_i\in S'\) in an execution of \(\rec\) is an element of \(G\).
	%A secret sharing instance \(\mathcal S = \left(S,\Gamma,G\right)\) has \emph{self-contained reconstruction}, if, for any \(S'\in\Gamma\) and any secret \(s\in G\), the input of each shareholder \(P_i \in S'\) to reconstructing \(s\) is in \(G\), so that reconstruction can be represented as an iterated application of the action of \(G\).
\end{definition}
%If the secret space is \(\Z_p\), this means that the shareholders' inputs are combined via addition.

It is necessary, that  \(G=\Z_p\) for some prime \(p\) holds to enable the mapping \(\cdot \mapsto [\cdot ]\). This requirement may be loosened by replacing \(\cdot \mapsto [\cdot]\) appropriately.
To enable two-level sharing, it has to hold that for a share \(s_i \in \mathcal S.\share\paren* s\) of a secret \(s\), \(s_i \in G\) holds.
%Also, for a secret \(s\) and any \(s_i \in \set{s_1,\ldots, s_k} \gets \mathcal S.\share\paren* s\), \(s_i \in G\) has to hold to enable key generation with two-level sharing.
The secret sharing scheme also has to allow for a PVP scheme, that is compatible with a zero-knowledge proof for the GAIP.

%And lastly, the access structure \(\Gamma\) for the secret sharing instance has to support superauthorised sets of shareholders. As we discussed in Section \ref{sec.kem}, this is necessary to ensure the soundness of the PVP.

\ifsubsection
\subsection{Examples of secret sharing schemes}
\else
\noindent\textbf{\\Examples of Secret Sharing Schemes.}
\fi

\begin{itemize}
	\item It is evident, that Shamir's approach fulfills all aforementioned requirements. In fact, the two-level sharing and the PVP have been tailored to Shamir's polynomial based secret sharing approach.
	\item Tassa \cite{DBLP:conf/tcc/Tassa04} extended Shamir's approach of threshold secret sharing to a hierarchical access structure. To share a secret \(s\in\Z_p\) with prime \(p\), a polynomial \(f\) with constant term \(s\) is sampled. Shareholders of the top level of the hierarchy are assigned interpolation points of \(f\) as in Shamir's scheme. The \(k\)-th level of the hierarchy receives interpolation points of the \(k-1\)st derivative of \(f\). The shares in Tassa's scheme are elements of \(\Z_p\) themselves. 
		The key generation (\algoref{fig.keygen}) can easily be transferred to this setting, as each shareholder receives a description of the polynomial utilised in sharing his share. Hence all derivatives and their respective interpolation points can easily be computed.
		%the shares The two-level sharing in the key generation protocol can be executed as given in \hyperref[fig.keygen]{Algorithm \ref{fig.keygen}}. For any shareholder, the polynomial, with which his share of the secret key was again shared, is handed to him. Thus all derivatives used in the sharing  are known the respective shareholder.
		Reconstructing a shared secret is achieved via Birkhoff interpolation, the execution of which is independent and self-contained. 
		The zero-knowledge proof (\algoref{fig.zkp} and \algoref{fig.zkv}) as well as the piecewise verifiable proof (\algoref{fig.tpvpp} and \algoref{fig.tpvpv}) thus directly transfer to Tassa's approach utilising the appropriate derivatives in the verifying protocols.
		The decapsulation and the signing protocols hence can be executed with adjustments only to the verifying steps.
	%\item Tassa's hierarchical secret sharing scheme \cite{DBLP:conf/tcc/Tassa04} is also based on sharing via a randomly sampled polynomial. To share a secret \(s\), a polynomial \(f\) in \(\Z_p\left[X\right]\) is sampled with constant term \(s\). Shareholders of the top rank in the hierarchy are assigned interpolation points of \(f\). The second rank is assigned points on the first derivative, in short, shareholders of the \(k\)-th rank obtain interpolation points of the \(k-1\)st derivative. With the shares being in \(Z_p\), this enables the necessary two-level sharing. The polynomial based sharing approach agrees with the PVP protocol given above with some minor adjustments. Thus, transferring the key exchange mechanism and the signature scheme to Tassa's secret sharing can easily be achieved.

	\item In 2006, Damgard and Thorbek proposed a linear integer secret sharing scheme \cite{DBLP:conf/pkc/DamgardT06} with secret space \(\Z\). Given an access structure \(\Gamma\), a matrix \(M\) is generated in which each shareholder is assigned a column so that iff \(S' \in \Gamma\), the submatrix \(M_{S'}\) has full rank. A secret \(s\) is shared by multiplying a random vector \(v\) with first entry \(s\) with \(M\) and sending the resulting vector entries to the respective shareholders. Reconstruction follows intuitively. Their scheme hence further generalises Tassa's with respect to secret space and feasible access structures. With the secret space \(\Z\) their approach is not compatible with the mapping \(\cdot \mapsto [\cdot]\) and our PVP scheme. Thus, neither our key exchange mechanism nor our signature scheme can in its current form be instantiated with  Damgard's and Thorbek's scheme.
		%In 2006, Damgard and Thorbek proposed a linear integer secret sharing scheme \cite{DBLP:conf/pkc/DamgardT06}. They enable a wide range of access structures by representing a given access strucure as a sharing matrix. For an integer secret \(s\) to be shared, a random vector with first entry \(s\) is sampled and multiplied with the sharing matrix. Thus reconstruction for an authorised set is achieved by inverting the corresponding submatrix corresponding and multiplying the set of their shares with the inverted matrix. With \(s\) and the shares being unbounded, Damgard's and Thorbek's scheme is not compatible with the mapping \(\cdot \mapsto [\cdot]\). Also their scheme does not comply with our PVP scheme. In its current form, our KEM and signature scheme cannot be instantiated with \cite{DBLP:conf/pkc/DamgardT06}'s approach. If a suitable PVP and substitution for \(\cdot \mapsto [\cdot]\) can be found, an instantiation with their scheme is feasible.

	%\item Additive secret sharing is the simplest of secret sharing schemes. For a given secret \(s\), each shareholder \(P_i\) receives a share \(s_i\), \(i =1 ,\ldots, n\), with \(s = \sum_{P_i} s_i\). Additive secret sharing has self-contained as well as independent reconstruction. Yet it is a full threshold secret sharing scheme, that is \(\Gamma = \set{S} = \set{\set{P_1,\ldots, P_n}}\). Thus for any \(P_i\in S\), the remaining shareholders \(\set{P_1,\ldots,P_n}\setminus \set{P_i}\) form an unauthorised set. Thus active security cannot be provided for the threshold group action, making additive secret sharing incompatible with our KEM and signature scheme.
\end{itemize}

