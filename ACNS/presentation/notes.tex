\documentclass[a4]{scrartcl}

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage[advantage,adversary,asymptotics,ff,keys,logic,mm,notions,operators,primitives,probability,sets]{cryptocode}
\DeclarePairedDelimiter{\paren}{\lparen}{\rparen}
\newcommand\keygen{\mathsf{KeyGen}}

\begin{document}
	\begin{enumerate}
% ------------------- Motivation -------------------
		\item Welcome and thank for your interest in our talk. We will in the next few minutes discuss our work "On Actively Secure Fine-Grained Access Structures from Isogeny Assumptions". This was joint work with Fabio Campos from RheinMain University in Wiesbaden.
		\item Let us first set the stage. We will be moving in the context of hard homogeneous spaces today. The concept of hard homogeneous space was first introduced by Couveignes in 2006. Such a space consists of a set \(\mathcal E\) and a group \(\mathcal G\) combined with an action \(\ast\), that combines an element of \(\mathcal E\) with an element of \(\mathcal G\) and produces another element of the set \(\mathcal E\).

			The mapping \(\ast\) has some important properties, that we will make use of in this work. First, we have compatibility. That is, if we take two element \(g\) and \(g'\) from \(\mathcal G\) and an arbitrary element of \(\mathcal E\), it does not matter, whether we first apply \(g'\) and then \(g\) to \(E\) or we first combine \(g\) and \(g'\).
			Second, we have the identity property. This means, that when we take the identity element of \(\mathcal G\) and combine it with an element of \(\mathcal E\), it stays unchanged. This must only hold for the identity element \(i\) of \(\mathcal G\).
			And third, we consider transitivity. This says, that for any two elements of \(\mathcal E\), there exists exactly one \(g\) in \(\mathcal G\), that connects them.

		\item We will use the bracket notation throughout this work, for which we fix an element \(g\) in \(\mathcal G\) with a prime order \(p\). For any \(s\) in \(\ZZ\) modulo \(p\) and any \(E\) in \(\mathcal E\), we then abbreviate \(g^s\) combined with \(E\) as \(\left[s\right] E\).

			The compatibility property then of course gives us the remarked equality.

			In a hard homogeneous space, we assume, that the group action inverse problem is hard to break. This means, that given two element \(E\) and \(E'\), the probability of providing a \(g\) that connects them both is negligible in an implied security parameter.

		\item We shall quickly cover secret sharing schemes, since everyone is likely to be familiar with the concept. We take a set of shareholders \(P_1,\ldots, P_n\). Among these a secret can be shared via the sharing protocol. And an authorised set of shareholders can reconstruct a secret from their shares via  the reconstruction protocol. In the case of Shamir sharing -- in which we give the basic version of our protocol -- this means that any set with at as many members as a fixed threshold can reconstruct the secret.

			We define the set of superauthorised sets of shareholders, that is sets, from which we can kick an arbitrary shareholder, and they are still authorised. We shall see the necessity of this definition later on.

		\item Now let us have a look at the definition of a key exchange mechanism. We have two parties, Alice and Bob. Alice has a secret and a public key. Alice publishes the public key and keeps the secret key to herself. Bob can now take the public key and run the so-called encapsulation protocol on it. This gives him a key \(\key\) and a ciphertext \(c\), that he can send to Alice. Alice in turn runs the decapsulation on the ciphertext with her secret key \(\sk\) and also gets a key \(\key\). The keys they both receive should coincide. We will take a closer look at the decapsulation protocol specifically in this work. 

		\item A key exchange mechanism in the context of a hard homogeneous space could look like this: For key generation, we sample the secret key \(\sk\) from \(\ZZ_p\). The public key is simply \(\sk\) applied to a publicly known and fixed \(E_0\).

			For the encapsulation protocol, we take a random \(b\in \mathcal G\). The ephemeral key \(\key\) is simply \(b\) connected to the public key and the ciphertext is \(b\) applied to \(E_0\).

			And decapsulation is handled by applying the secret key to the ciphertext. 
			
		\item Let us consider the following setting: Alice does not hold the secret key herself, but it is shared in a secret sharing scheme. This does have some advantages. For example, she cannot lose the key or leak it. On the other hand, she must always ask the shareholders for the key, if she wants to decapsulate a ciphertext. Or does she? It turns out, that she does not. DeFeo and Meyer proposed a protocol in a Shamir setting, in which a decapsulation can be executed by the shareholders without reconstructing the secret key. For that we assume an authorised set of shareholders, say  \(S'\), comes together and receives a ciphertext as input. Each shareholder holds a share \(s_1\) to \(s_4\) of the secret key. They fix a turn order and one after the other apply their share to the respective input. More precisely, the first shareholders applies \(L_{1,S'} s_1\) to the ciphertext and passes the result on to the second shareholder. He in turn applies \(L_{2,S'} s_2\) and so on. \(L_{i,S'}\) denotes the Lagrange interpolation coefficient. The last shareholder's output is then the result of the decapsulation. 

		\item But why does this work? We take a quick look at the so-called threshold group action. We see, that the result of this round-robin approach turns out to be simply the reconstructed secret key to the ciphertext. So actually, this is the previous decapsulation, but executed by a set of shareholders instead of the owner of the key.

			DeFeo and Meyer showed, that this protocol is simulatable, even if we suspect, that their proof was not entirely correct, the statement nevertheless holds true. Their approach has two nice features, that is you do not need all shareholders but only an authorised set. And their turn order is not fixed, but can be arranged in  any suitable way.

			The problem however is, that their protocol is only passively secure.

		\item That is, a misbehaving shareholder, as we depicted here, cannot be detected. One, because his share is obviously unknown to all other parties. And two, it cannot be computed from his input and output.

% ------------------- Actively Secure Key Exchange Mechanism -------------------
		\item We will use two measures to achieve active security in the decapsulation protocol. 
			First, we have a zero-knowledge proof of knowledge in the context of a hard homogeneous space. For that we take a secret \(s\) and a list of tuples \(\left(E_i,E_i'\right)\) in \(\mathcal E\). Each \(E_i'\) results from applying \(s\) to \(E_i\). A prover thus proves knowledge of \(s\) beyond reasonable doubt without revealing \(s\). The protocol for this is non-interactive.

			And second, we use a piecewise verifiable proof. Consider for that a statement \(x\), that consists of a pair \(\left(E_0,E_1\right)\) and a list of \(s_i\)'s in \(\ZZ_p\). A PVP proves knowledge of a polynomial \(f\) in \(\ZZ_p\), that connects \(E_0\) and \(E_1\) and also interpolates the values \(s_i\). The protocol for this is again non-interactive.

		\item So the challenges that we face are the following.
			One, the PVP as stated before and discussed by Beullens et al. does not consider the threshold setting. It always proves each interpolation point and does not consider, that not a secret, but a multiple of it is used in the decapsulation. We need to amend that to fit our needs.

			Two, we must marry the zero-knowledge proof and the PVP with the decapsulation protocol in order to achieve active security.

			And three, we should prove, that the protocol, that we get from this, is simulatable, as the original protocol by DeFeo and Meyer was.

		\item We skip the precise modified protocols, since they do not offer much insight. The changes we had to make were mostly on the technical side.
		
			Now, for our key exchange mechanism we need a key generation protocol, an encapsulation and a decapsulation protocol. Let us start with the key generation protocol. This looks quite a bit different from the one we showed before. We apply, what we call a "two-level" sharing. That is, we let the dealer sample the secret key and compute the public key as before. The secret key is then shared among \(P_1,\ldots,P_n\). But each share of \(\sk\) is shared once more by the dealer, so that each shareholder eventually receives his share \(s_i\) of the secret key, the polynomial \(f_i\), with which his share was shared once more and a share of each other share of the secret key.

		\item The encapsulation protocol stays unchanged, since the secret shared secret key does not concern it.

		%superauthorised set
		\item The decapsulation protocol looks quite different from the one we sketched before. We show the turn of a shareholder \(P_i\), that is engaged in an execution of the decapsulation protocol. Let us say, that \(P_i\) is the \(k\)-th shareholder in the turn order and that a superauthorised set of shareholders executes the decapsulation.

			\(P_i\) gets \(E^{k-1}\) as input either with \(E^0\) being the ciphertext or the output of the shareholder, whose turn is was before. If \(E^{k-1}\) is not in \(\mathcal E\), then \(P_i\) simply stops and the protocol aborts.

			Otherwise, \(P_i\) samples a random \(R_k\) from the set \(\mathcal E\) and computes \(R_k'\) by applying \(L_{i,S^\ast}\) to \(R_k\).

			Now \(P_i\) generates a piecewise verifiable proof, that proves knowledge of the polynomial \(f_i\), so that \(L_{i,S^\ast} f_i\paren* 0\) connects \(R_k\) and \(R_k'\) and interpolates the values \(f_i\paren* j\), of all other participating shareholders. Furthermore, \(P_i\) computes his contribution to the decapsulation \(E^k\) as he did in the passively secure protocol. And he computes the zero-knowledge proof, that shows, that he knows \(L_{i,S^\ast} s_i\) and also, that both \(E^k\) and \(E^{k-1}\) as well as \(R_k\) and \(R_k'\) are connected by it. In combination, this proves that \(R_k\) and \(R_k'\) and \(E^{k-1}\) and \(E^k\) are connected by the same element, and that the connecting element coincides with the one, that was shared by the dealer.

		\item As a fourth step, all other participants, verify the proofs, that \(P_i\) has published.

			If \(P_i\) misbehaved, the protocol is restarted and \(P_i\) is excluded from future runs.

			The last shareholder eventually outputs \(E^{\# S^\ast}\) as the result of the decapsulation.

		\item Our key exchange mechanism has the following qualities. It is IND-CPA. This means, that given a public key \(\pk\), a ciphertext \(c\) and two potential keys, an adversary cannot distinguish which one is linked to the ciphertext. We skip the precise security game here.

			Also, our decapsulation protocol is simulatable. We proved this in reducing the group action inverse problem to distinguishing the output of a concrete simulator from a real transcript in a series of gamehops.
			 
			 And third, the decapsulation is actively secure, in that if a shareholder can provide an incorrect input to the decapsulation without being detected by the other participants, then he can either the zero-knowledge proof or the piecewise verifiable proof.

		 \item We come to a little bonus, that we derive from our key exchange mechanism. We apply the Fiat-Shamir-transform to our scheme to obtain an actively secure signature scheme. One may wonder, what why one would need an actively secure signature scheme. After all, if a signature was computed incorrectly, this can obviously be seen, from the verification of the signature failing. But in a setting, where multiple shareholders participate generating a signature, this does not identify the shareholder, who misbehaved. Our protocol can do just that.

			 For a signature scheme, we need the protocols. A key generation protocol, a signing protocol and a verifying protocol. We simply keep the \(\keygen\) protocol from the key exchange mechanism. That is, a dealer samples a secret key, computes and publishes the public key and shares the secret key among the shareholders in a two-level sharing. We then apply the Fiat-Shamir-transform to our decapsulation protocol. Traditionally, this transform is to be applied to identification schemes, not key exchange mechanisms. One can however consider our decapuslation protocol as an identification scheme in that a successful decapsulation identifies the executing set of shareholders as authorised, that is it holds the secret key. This gives us a non-interactive signing protocol. The verifying protocol comes naturally with the signing protocol.

		 \item We come to our last contribution and -- to be honest -- the initial motivation for this work. We discuss, which secret sharing schemes we can instantiate our key exchange mechanism with other than the traditional Shamir scheme. For that, we first define the characteristics, that a secret sharing must have to be compatible with our protocols.
			 First, it needs independent reconstruction. This means, that the input of a shareholder in reconstructing a secret must not depend on what the other shareholders gave as input. We need this, since each share is hidden due to the group action inverse problem, so no participant can see any share but his own.
			 Second, we need self-contained reconstruction. This enables the two-level sharing, for which the shares of a secret live in the same space as the secret so that they can be shared once more.
			 And third the secret sharing scheme must be compatible with the zero-knowledge proof and the piecewise verifiable proof in the hard homogeneous space.

		 \item Let us have a quick look at some examples of secret sharing schemes and see, whether they are compatible with our key exchange mechanism.
			 Shamir's secret sharing scheme obviously is compatible, we gave our protocols in the context of it.

			 Tassa gave a fascinating extension of Shamir's approach in his hierarchical threshold secret sharing scheme. It is also based on polynomial interpolation, yet the lower in the hierarchy you go, the higher the degree of the derivation of the original polynomial becomes, of which a shareholder gets interpolation points. It is directly compatible with our protocols, be it with some tweaks to the PVP and the zero-knowledge proof.

			 Damgard and Thorbek proposed a secret sharing scheme, in which integer secret rather than secrets from \(\ZZ_p\) are shared. The confidentiality of their scheme is only computational, so we deem it incompatible with  our protocol.

			 The simplest of all secret sharing schemes, that is additive secret sharing, is incompatible with our key exchange mechanism, since it cannot provide any superauthorised sets and therefore does not enable a piecewise verifiable proof.

		 \item In conclusion, we proposed an actively secure key exchange mechanism in which the secret key is shared among a set of shareholders, that enables decapsulation without reconstructing the secret key. We proved the decapsulation protocol simulatable and actively secure, that is an adversary cannot learn any information from an execution and cannot interfere by providing false input without detection. The key exchange mechanism is also indistinguishable under chosen message attack, that is nothing can be learned about an encapsulated key from the corresponding ciphertext.

			 For this aim, we transferred the PVP and zero-knowledge proof to the threshold setting.

			 We furthermore transformed the key exchange mechanism into a signature scheme with active security.

			 And lastly, we characterised the properties, that a secret sharing scheme has to have in order to be compatible with the key exchange mechanism and the signature scheme.
	\end{enumerate}

\end{document}

