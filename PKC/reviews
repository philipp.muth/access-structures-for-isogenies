----------------------- REVIEW 1 ---------------------
SUBMISSION: 2
TITLE: On Actively Secure Fine-grained Access Structures from Isogeny Assumptions
AUTHORS: Philipp Muth and Fabio Campos

----------- Overall evaluation -----------
SCORE: 1 (weak accept)
----- TEXT:
The paper is about threshold isogeny-based cryptography, in particular threshold decryption/decapsulation. Public key is pk=[s]E_0, ciphertext is C=[b]E_0. Decapsulation is to compute [s]C.

In the threshold setting player i has share s_i such that s = \sum_{i in S} L_{i,S}*s_i. So for decap player i needs to compute [L_{i,S}*s_i]*C.

De Feo and Meyer [11] showed a general approach to get threshold CSIDH-KEM and CSI-FiSh signatures. Their protocols are only passively secure. The basic operation in these schemes is to compute [s]*E where s is Shamir-secret-shared among the players. Beullens, Disson, Pedersen and Vercauteren [2] showed how to make this basic operation [s]*E actively secure. Why is it not immediate that [2]+[11] provides secure threshold KEM and signature? Page 8 of the paper seems to explain what is the problem ("This approach does not agree with the threshold group action, for which a shareholder P_i's output in the round-robin approach is E_k = [L_{i,S}s_i]E_{k-1} rather than E_k = [s_i]E_{k-1}"), but I am not sure quite why this is not solved in [2], since they also use polynomial secret sharing.

The paper claims four original contributions:
1. transfer the active security measures outlined in [2] from their setting of full engagement protocols to a setting of threshold secret sharing.
2. apply the adapted active security measures to propose an actively secure key exchange mechanism with secret shared secret key. 
3. present an actively secure signature scheme by applying a Fiat-Shamir transform to our key exchange mechanism. 
4. expand our key encapsulation mechanism and our signature scheme to a wider ﬁeld of secret sharing schemes.

Contributions 1 and 2 seem to me to be fairly straightforward combinations of [2] and [11], but I might be missing something. Item 3 is strangely worded. As far as I can see you do not "apply Fiat-Shamir to your key exchange mechanism". Your signature is threshold CSI-FiSh, as in [11]. Again seems to be just [2]+[11. Finally contribution 4 is essentially trivial: to note that the PVP in [2] can also be used with Tassa's approach.

One change to the protocol is the requirement that decapsulation is executed by superauthorised sets S^*. Hence t+1 players are requred to get active security, when t players would be enough to decapsulate with passive security.

The paper does not do a good job of giving the reader intuition for the two-level sharing s_{ij} and the role that it plays in PVP.V (Algorithm 9). If I understand right, since for decap player i needs to compute [L_{i,S}*s_i]*C we need to show correctness of [L_{i,S}*s_i]E rather than [s_i]E as in other works. This is done quite easily, since L_{i,S} is a public constant, by sharing s_i as s_{ij}, from which L_{i,S}*s_{ij} are shares of L_{i,S}*s_i. In my opinion, introducing f_i to share s_i and then defining f_i' = L_{i,S}*f_i is unnecessarily complicated and it would have been simpler to just share L_{i,S}*s_i as s_{ij}. 

Indeed, this seems to lead to typos. On page 10 you say the \pi^k_j come from PVP(f_i,S^*,(R_k,R_k'), s_{ij} ), where R_k' = [L_{i,S'}*s_i]R_k = [f_i'(0)]R_k. But I feel it should be PVP(f_i',S^*,(R_k,R_k'),L_{i,S'}*s_{ij}). It seems to be correct in the security proof on page 13 (step 1).

The key idea, if I understand right, is that player j gets s_{kj} for all users k, and is thus able to check the commitments in the j-th proof component \pi^k_j. This is somewhat buried in the details (eg Algorithm 9 in the Appendix).

Overall, the paper seems ok. But there is room for improvement in the writing. Please try to fix the structure of the paper to make it more clear. Perhaps after a few weeks away from the paper you will be able to see how some parts are confusing and can be improved. If not, please ask your friends and colleagues for their advice.

Some comments on the English:

- You use the word "falsify" for cheating behavior (e.g., on page 6 and 7 "falsify the output without being detected"). Is this standard terminology? I am not familiar with it, and the meaning is not obvious. I guess "falsify" means "make the output wrong". Also, as you discuss on page 15, "detected" is weaker than you want. You want the cheater to be *identified*.

- You sometimes use too many commas. For example commas are not needed in "We show, that an adversary", "call a key exchange mechanism simulatable, if, for any", "This series is inspired by the simulators, that [2] gave"



----------------------- REVIEW 2 ---------------------
SUBMISSION: 2
TITLE: On Actively Secure Fine-grained Access Structures from Isogeny Assumptions
AUTHORS: Philipp Muth and Fabio Campos

----------- Overall evaluation -----------
SCORE: 2 (accept)
----- TEXT:
This article describes isogeny-based key exchange and signature schemes based on Shamir's secret sharing scheme, adapted to cyclic group actions in Couveignes' HHS framework.  CSIDH, a post-quantum group-action candidate, is the obvious and targeted instantiation.

In fact, the setting is closer to that of CSI-FiSh than pure CSIDH: we need an element of the class group of large prime order, which strongly suggests that we need the factorization of the class group order.  This is known for CSIDH-512, but (as the authors rightly note) the true security level of CSIDH-512 is in question.  However, computing class group orders for CSIDH is a subexponential algorithm, and the computation for CSIDH-512 already represents a world record in that area.  It is not clear to me that we will ever be able to compute class group orders for the larger CSIDH parameters that will be required for better security: if we have to move to 1024-bit field primes, for example, then we have no chance of computing the class group.

We are therefore in the domain of cryptosystems that may never be securely realised, which is a massive caveat.

Setting this issue to one side, given a choice of element g of order p, where p is a large prime dividing the group order, we get an action of ZZ/pZZ on the HS defined by (n,E) -> g^n * E .  Now Shamir's secret sharing can be done on the integer multipliers, because the action is additively homomorphic.  This was done by De Feo and Meyer, but they only achieved security against passive adversaries; the real contribution of this work is to define similar schemes secure against active attacks.

The schemes in question are
- a KEM whose private key is shared among a set of shareholders, and can be reconstructed by authorised subsets of users to decapsulate;
- a signature scheme with a shared secret signing key.  (Active security in this context means that cheating shareholders can be identified and excluded from future signings.)

The article also discusses some (im)possible generalizations.  Briefly: the results extend nicely from Shamir to Tassa's hierarchical access structures; but they do not extend to Damgard and Thorbek's even more general secret sharing.

I found this article clearly and logically set out, and very easy to read (for what is mostly provable security), though I lack expertise with these kinds of proofs and could not confidently verify them in full detail.  The results are practical insofar as CSIDH-512 is acceptable, but unfortunately the above caveat applies.  In any case, they are a good advertisement for the sorts of advanced cryptographic constructions that can be easily achieved within the group-action framework for post-quantum security.  It should be interesting, and accessible, to a large part of the PQCrypto audience.



----------------------- REVIEW 3 ---------------------
SUBMISSION: 2
TITLE: On Actively Secure Fine-grained Access Structures from Isogeny Assumptions
AUTHORS: Philipp Muth and Fabio Campos

----------- Overall evaluation -----------
SCORE: 1 (weak accept)
----- TEXT:
This paper proposes a threshold secret sharing scheme for CSIDH key exchange and then extends this construction to arbitrary access structures as well as signature schemes providing active security. The result improves upon previous results, which did not achieve this particular combination -- prior schemes either did not support arbitrary access structures, or did not achieve active security (in the sense of being able to identify cheating shareholders in real time).

The corresponding results for regular discrete logarithm cryptography are already known. The main contribution here is transporting those results from the setting of discrete logarithms to that of hard homogeneous spaces. It is not so surprising to me that the authors managed to accomplish this task -- intuitively if you can treat and represent CSIDH-512 as a cyclic group action then the modifications to convert (say) a threshold access structure to this setting seem very natural. Furthermore there is currently no realizable instantiation of cyclic HHSs other than CSIDH-512, which as the paper points out has weak post-quantum security, although when quantum computers become available we will be able to pre-compute the data needed for larger parameters. The generalizations to other secret sharing schemes is a nice addition, but still I feel the result just seems weak compared to other submissions.



