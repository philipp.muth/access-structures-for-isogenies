\section{Preliminaries}\label{sec.prelim}

Throughout this work we use a security parameter \(\lambda\in \N\). It is implicitly handed to a protocol whenever needed, that is protocols with computational security. Information theoretic schemes and protocols such as secret sharing schemes used in this work are not affected by this.

\subsection{Secret Sharing Schemes}
A secret sharing scheme is a cryptographic primitive to distribute a secret \(s\) from a secret space among a set of shareholders. An instance \(\mathcal S\) is defined by a secret space \(G\), a set of shareholders \(S\) and an access structure \(\Gamma_{\mathcal S} \subset 2^S\). A set \(S'\in\Gamma\) is called \emph{authorised} and can from their respective shares reconstruct a shared secret. If the instance \(\mathcal S\) is clear from the context, we omit the index in the access structure \(\Gamma\). In this work, we consider monotone access structures, that is for any \(A,B \subset S\) with \(A\in\Gamma\) and \(B\supset A\), we also have \(B \in \Gamma\).

A secret sharing instance \(\mathcal S\) provides two algorithms: \(\share\) and \( \rec\). A dealer executes \(\mathcal S.\share\paren* s\) to generate shares \(s_1,\ldots, s_k\) of a secret \(s\). A share \(s_i\) is assigned to a shareholder \(P_{\phi\paren* i}\) via a surjective map \(\phi : \set{1,\ldots, k} \to \set{1, \ldots,n}\) induced by \(\Gamma_{\mathcal S}\). A set of shareholders \(S'\in \Gamma_{\mathcal S}\) executes 
\[\mathcal S.\rec\paren*{\set{s_i}_{P_{\phi\paren* i} \in S'}}\]
on their respective shares to retrieve a previously shared secret.

\begin{defin}[Superauthorised sets]
	For a secret sharing instance \(\mathcal S =\left(G,S,\Gamma_{\mathcal S}\right)\), we call a set of shareholders \(S'\) \emph{superauthorised}, if for any \(P\in S'\), we have 
	\[S' \setminus \set P \in \Gamma_{\mathcal S}.\]
	The set of superauthorised sets of shareholders is denoted by \(\Gamma_{\mathcal S}^+\).
\end{defin}
Any superauthorised set is also authorised.

\begin{exm}[Shamir's secret sharing]
	Shamir's secret sharing scheme is defined by \(S=\set{P_1, \ldots, P_n}\), the access structure \(\Gamma = \set{S' \subset S \colon \# S' \geq t}\) for some fixed threshold \(1\leq t \leq n\) and the secret space \(G = \Z_p := \Z \bmod p\)  for some prime \(p > n\). To share a secret \(s \in \Z_p\), a random polynomial \(f \in \Z_p \left[ X \right]\) of degree \(t-1\) with constant term \(s\) is sampled and the shares are defined by
	\[ s_i = f\paren* i\]
	for \( i = 1, \ldots, n\). The assigning function \(\phi\) is simply the identity function, thus \(P_i\)'s share is \(f\paren* i\), \(1\leq i\leq n\). Reconstruction for an authorised set \(S'\) is achieved via Lagrange interpolation, that is
	\[ s = \sum_{P_i \in S'} L_{i,S'} s_i = \sum_{P_i \in S'} \prod_{\substack{P_j \in S'\\ j\neq i}} \frac{j}{j-i} f\paren* i, \]
	where \(L_{i,S'}\) denotes the Lagrange interpolation coefficients.

	The superauthorised sets \(\Gamma^+\) are
	\[\set{S' \subset S \colon \# S' > t}.\]
\end{exm}

%In this work, however, we assume, that \(\mathsf{input}_i\) only depends on \(P_i\)'s share and \(S^\ast\) (which in both examples above is satisfied). Furthermore, we assume the secret sharing schemes to agree with the group action of the group \(G\), form which the secret are picked. That is, \(\mathcal S\) provides an algorithm \(\add\), that has the shareholders combine their shares of two shared secret \(s, s'\in G\) and obtain a share of \(s \ast s'\). In short, for an authorised set of shareholders \(S^\ast \in \Gamma\), we have
%\[ \rec\paren*{\add\paren*{\set*{s_i}_{P_i \in S^\ast},\set*{s'_i}_{P_i \in S^\ast}}} = \rec\paren*{\set*{s_i}_{P_i \in S^\ast}} \ast \rec\paren*{\set*{s'_i}_{P_i \in S^\ast}}, \]
%where \(\left(s_1, \ldots, s_{\#S}\right) \gets \share\paren* s\) and \(\left(s'_1, \ldots, s'_{\#S}\right) \gets \share \paren*{s'}\).

%We consider some additional properties of secret sharing schemes, that come into use when extending the key exchange mechanism so accommodate more general secret sharing schemes.

\subsection{Hard Homogeneous Spaces}
We present our key exchange mechanism and signature scheme in the context of \emph{hard homogeneous spaces} (HHS). HHS were first discussed by Couveignes \cite{EPRINT:Couveignes06} in 2006. He defines a HHS \(\left(\mathcal E, \mathcal G\right)\) as a set \(\mathcal E\) and a group \(\left(\mathcal G, \odot\right)\) equipped with a transitive action \( \ast : \mathcal G \times \mathcal E \to \mathcal E\). This action has the following properties:

\begin{itemize}
	\item Compatibility: For any \(g, g'\in \mathcal G\) and any \(E\in \mathcal E\), we have \(g \ast \left( g'\ast E\right) = \left(g\odot g'\right) \ast E\).

	\item Identity:	For any \(E\in\mathcal E\), \(i \ast E = E\) if and only if \(i\in\mathcal G\) is the identity element.

	\item Transitivity: For any \(E,E' \in \mathcal E\), there exists exactly one \(g \in \mathcal G\) such that \(g \ast E = E'\).

\end{itemize}
For ease of notation we define the following.
\begin{defin}[Notation]
	For a secret sharing instance \(\mathcal S\) with secret space \(\Z_p\) and a hard homogeneous space \(\left(\mathcal E,\mathcal G\right)\), where \(p \vert \# \mathcal G\), we fix  a \(g\in \mathcal G\) with order \(p\) and denote from now on
	\[\left[s\right] E := g^s \ast E\]
	for all \(s \in \Z_p\) and all \(E \in \mathcal E\).
\end{defin}

\begin{comment}
\subsubsection{Hard Problems}
\begin{figure}
	\begin{center}
	\procedure[linenumbering, space = auto]{$\mathsf{Exp}^\text{P-DDHA}_{\left(\mathcal E, \mathcal G\right),\adv} \paren*{\lambda}$}{
		b\sample \bin\\
		E \sample \mathcal E\\
		a \sample \set{2, \ldots, \# G-1}\\
		\mathfrak s \sample \mathcal G\\
		\pcif b = 0\\
			F \gets \mathfrak s^a \ast E\\
		\pcelse\\
			F \sample \mathcal E\\
		\pcfi\\
		b' \gets \adv \paren*{\left(a,E,\mathfrak s\ast E, F\right)}\\
		\pcreturn \left( b = b'\right)
	}
	\end{center}
	\caption{The Power-DDHA game}
	\label{fig:gamepddha}
\end{figure}

\begin{problem}[Power-DDHA]
	For an \(E \in \mathcal E\), \(\mathfrak s \in \mathcal G\) and \(a \in \set{2, \ldots, \#G-1}\) chosen uniformly at random, the tuple \(\left(a, E, s\ast E, F\right)\) defines an instance of the \emph{Power-DDHA problem}, where either \(F = \mathfrak s ^a \ast E\) or \(F\) was drawn uniformly at random from \(\mathcal E\). The challenge is for an adversary \(\adv\) to determine whether \(F = \mathfrak s^a \ast E\) or \(F\) was drawn from \(\mathcal E\) uniformly at random.
\label{prob:pddha}\end{problem}
A sketch of the problem can be found in \autoref{fig:gamepddha}.
\begin{defin}
	For an instance \(\left(a,E,s\ast E, F\right)\) of the Power-DDHA problem, we define the adversary \(\adv\)'s advantage in the \(\mathsf{Exp}^\text{P-DDHA}_{\left(\mathcal E, \mathcal G\right),\adv} \) game as
	\[\advantage{\text{P-DDHA}}{\left(\mathcal E, \mathcal G\right), \adv}[\paren* \lambda] = \abs{ \frac 12 - \prob{\mathsf{Exp}^\text{P-DDHA}_{\left(\mathcal E, \mathcal G\right),\adv}  = \true}}. \]
\end{defin}
\end{comment}

The following problems are assumed to be easily computable in a HHS \(\left(\mathcal E, \mathcal G\right)\), i.e., there exist polynomial time algorithms to solve them:

\begin{itemize}
	\item Group operations on \(\mathcal G\) (membership, inverting elements, evaluating \(\odot\)).

    \item Sampling elements of \(\mathcal E\) and \(\mathcal G\).

    \item Testing the membership of \(\mathcal E\).

	\item Computing the transitive action \(\ast\): given \(g\in \mathcal G\) and \(E\in\mathcal E\) as input, compute \(g \ast E\).
\end{itemize}

Whereas the subsequent problems are assumed to be hard in a HHS \(\left(\mathcal E,\mathcal G\right)\).

\begin{problem}[Group Action Inverse Problem (GAIP)]
	Given two elements \(E,E'\in\mathcal E\) as input, the challenge is to provide \(g \in \mathcal G\) with \(E'= g \ast E\).

	Due to the transitivity property of hard homogeneous spaces, such a \(g\) always exists, thus any given instance of the GAIP has a solution.
\label{prob:gaip}\end{problem}

\begin{problem}[Parallelisation Problem]
	An instance of the \emph{Parallelisation Problem} is defined by a triple \(\left(E, E', F\right)\in \mathcal E^3\) with \(E' = g\ast E\). The challenge is to provide \(F'\) with \(F' = g\ast F\).
\end{problem}
The intuitive decisional continuation of this problem is as follows.
\begin{problem}[Decisional Parallelisation Problem]
	An instance of the \emph{Decisional Parallelisation Problem} is defined by a base element \(E\in\mathcal E\) and a triple \(\left(E_a,E_b,E_c\right)\) with \(E_a = [a]E\), \(E_b= [b]E\) and \(E_c = [c]E\). The challenge is to distinguish whether \(c=a+b\) or \(c\sample \Z_p\) was randomly sampled.%Given two tuples \(\left(E,E'\right)\) and \(\left(F,F'\right) \in \mathcal E^2\) as input, where \(E' = g\ast E\), the \emph{Decisional Parallelisation Problem} (DPP) is to determine whether \(F' = g\ast F\) or \(F \sample \mathcal E\).
\end{problem}
%The DPP can be rephrased as being defined by a base element \(E\in \mathcal E\) and a triple \(\left(E_a, E_b,E_c\right)\) with \(E_a = \left[a\right] E\), \(E_b = \left[ b \right] E\) and \(E_c = \left[c\right] E\). The challenge is to distinguish \(c = a b\)  from \(c\) being picked at random.

\begin{bem}
	It is obvious that the decisional parallelisation problem reduces to the parallelisation problem, which in turn reduces to the group action inverse problem.
\end{bem}

\begin{comment}
\begin{problem}[DDH in HHS]
	An instance of \emph{DDH} in a hard homogeneous space \(\left(\mathcal E, \mathcal G\right)\) is defined by a tuple \(\left(a, E, F\right)\), where \(1< a < \#G\) and \(E, F \in \mathcal E\). An adversary's challenge is to determine whether \(F = s^a \ast E\) for some \(s\in \mathcal G\) or \(F\) was drawn from \(\mathcal E\) uniformly at random.
\label{prob:ddh}\end{problem}

\begin{bem}
	It is evident, that an instance of Power-DDHA \(\left(a, E ,\mathfrak s^a \ast E\right)\) can easily be reduced to distinguishing tuples  \(\left(E,F\right)\), where \(F \sample \mathcal E\) is either sampled randomly from \(\mathcal E\) or \(F = \mathfrak s^a \ast E\). %Given an efficient distinguisher \(\mathcal D\) for tuples \(\left(E,F\right)\) and \(\left(E,s^a\ast F\right)\), we construct an adversary \(\mathcal D'\) against Power-DDHA by simply having \(\mathcal D'\) pass on the tuple \(\left(E,F\right)\) to \(\mathcal D\) and outputting whatever \(\mathcal D'\) outputs.
	
\end{bem}
\end{comment}


\subsection{Threshold Group Action}
Assume, that a secret \(s\) has been shared in a Shamir secret sharing instance, thus each shareholder \(P_i\) holds a share \(s_i \) of \(s\), \(i=1,\ldots, n\). Let \(E\) be an arbitrary but fixed element of \(\mathcal E\). The action \(E' \gets \left[s\right] E\) can be computed by any authorised set \(S'\) without reconstructing \(s\) by executing  the protocol given in \algoref{fig.tga}.
%\begin{center}
% \begin{figure}
% 	\procedure[space = auto, linenumbering]{Threshold Group Action}{
% 		E^0 \gets E; k \gets 0\\
% 		\pcfor P_i \in S^\ast\\
% 			\pcif E^k \not \in \mathcal E\\
% 				P_i \text{ outputs } \bot \text{ and aborts.}\\
% 			\pcelse\\
% 				k \gets k+1\\
% 				P_i \text{ outputs }E^k \gets \left[L_{i,S^\ast} s_i \right] E^{k-1}\\
% 			\pcfi\\
% 		\pcendfor\\
% 		\pcreturn E^k
% 	}
% 	\caption{Threshold group action protocol}
% 	\label{fig.tga}
% \end{figure}

\begin{algorithm}
    \DontPrintSemicolon
    \SetAlgoShortEnd
    \SetAlgoVlined
    \SetInd{0.5em}{1em}
    \KwIn{$E, S'$}
    %\KwOut{a public message $M$}
    $\assignTo{E^0}{E}$\;
    $\assignTo{k}{0}$\;
    % $\text{parse } \left(y_j, y_j'\right) \gets \pi_j$\;
    % $c_1, \ldots, c_\lambda \gets \mathcal H \paren*{C,C'}$\;
    % $\assignTo{K}{0}$\;
    % $\assignTo{P}{0}$\;
    \For{$P_i \in S'$}{
        \If{$E^k \not \in \mathcal E$}{
            $P_i \text{ outputs } \bot \text{ and aborts.}$\;
        }\Else{
            $\assignTo{k}{k+1}$\;
            $P_i \text{ outputs }E^k \gets \left[L_{i,S'} s_i \right] E^{k-1}$\;
        }
    }
    \Return{$E^k$}
    % \Return{\begin{split}C_j == \mathcal C \paren*{r_1 \paren* j + c_1 \cdot L_{i,S'}\cdot x_j \concat \ldots\concat r_\lambda\paren* j + c_\lambda \cdot L_{i,S'}\cdot x_j,y_j}\end{split}}
    \caption{Threshold group action}
    % \label{alg:exampleVulnerable}
    \label{fig.tga}
\end{algorithm}

%\end{center}
If \algoref{fig.tga} is executed without aborting, we have by the compatibility property of \(\ast\) and the repeated application of 
\[E^k \gets \left[L_{i,S'} s_i\right]E^{k-1}\]
the result 
\[E^{\# S'} = \left[\sum_{P_i \in S'} L_{i,S'} s_i \right] E = \left[ s\right] E.\]

\subsection{Piecewise Verifiable Proofs}
A piecewise verifiable proof (PVP) is a cryptographic primitive in the context of hard homogeneous spaces and was first introduced in \cite{EPRINT:BDPV20}. It is a compact non-interactive zero-knowledge proof of knowledge of a witness \(f \in \Z_q\left[X\right]\) for a statement
\begin{equation}
	\left(\left(E_0,E_1\right), s_1, \ldots, s_n\right),
\label{eq.pvprelation}\end{equation}
where \(E_1 = \left[s_0\right] E_0 \in \mathcal E\) and \(s_i = f\paren* i\) for  \(i = 0, \ldots, n\). A PVP provides two protocols. The proving protocol \(\mathsf{PVP}.P\) takes a statement \(x\) of the form \eqref{eq.pvprelation} and a witness \(f\) as input and outputs a proof \(\left(\pi,\set{\pi_i}_{i=0,\ldots, n}\right)\), where \(\left(\pi,\pi_i\right)\) is a proof piece for the partial statement \(x_i\), \(i = 0,\ldots, n\). The verifying protocol \(\mathsf{PVP}.V\) takes an index \(i \in \set{0,\ldots, n}\), a statement piece \(x_i\) and a proof piece \(\left(\pi,\pi_i\right)\) as input and outputs \(\true\) or \(\false\). 

Let \(\mathcal R\) denote the set of all tuples \(\set{\left(x,f\right)}\), where \(f\) is a witness for the statement \(x\). Furthermore, for \(I\subset\set{0,\ldots,n}\), we let \(\mathcal R_I\) denote the set of partial relations \(\set{\left(x_I,f\right)}\), where there exists \(\left(x,f\right) \in \mathcal R\) so that \(x\vert_I = x_I\).

\begin{defin}[Completeness]
	We call a PVP \emph{complete}, if, for any \(\left(x,f\right)\in \mathcal R\) and
	\[\left(\pi,\set{\pi_i}_{i = 0,\ldots, n}\right) \gets \mathsf{PVP}.P\left(f,x\right),\]
	the verification succeeds with overwhelming probability, i.e.,
	\begin{align*}
		\prob{\mathsf{PVP}.V\left(j,x_j,\left(\pi,\pi_j\right)\right) = \true} = 1
	\end{align*}
	for all \(j \in \set{0,\ldots, n}\).
\end{defin}

\begin{defin}[Soundness]
	A PVP is called \emph{sound} if, for any adversary  \(\adv\), any \(I \subset \set{0,\ldots, n}\) and any statement \(x\) for which there exists no witness \(f\) with \(\left(x_I,f\right) \in \mathcal R_I\), 
	\[\prob{\mathsf{PVP}.V\paren*{j,x_j,\left(\pi,\pi_j\right)} = \true }\]
	is negligible in the security parameter \(\lambda\) for all \(j \in I\), where \(\left(\pi,\set{\pi_i}_{i\in I}\right)\gets \adv\paren*{1^\lambda}\).
\end{defin}

\begin{defin}[Zero-knowledge]
	A PVP is \emph{zero-knowledge}, if for any \(I \subset \set{1,\ldots, n}\) and any \(\left(x,f\right)\in \mathcal R\), there exists a simulator \(\simul\) such that for any polynomial-time distinguisher \(\adv\) the advantage
	\[ \left\lvert \prob{\adv^{\simul\paren*{x_I}}\paren*{1^\lambda} = 1}  - \prob{\adv^{P\paren*{x,f}}\paren*{1^\lambda} = 1} \right\rvert\]
	is negligible in the security parameter \(\lambda\), where \(P\) is an oracle that upon input \(\left(x,f\right)\) returns \(\left(\pi,\set{\pi_j}_{j \in I}\right)\) with \(\left(\pi,\set{\pi_j}_{j =  0, \ldots, n}\right)\gets\mathsf{PVP}.P\paren*{f,x}\).
\end{defin}
We refer to \cite{EPRINT:BDPV20} for the precise proving and verifying protocols and the security thereof. In combination they state a complete, sound and zero-knowledge non-interactive PVP.

A prover can hence show knowledge of a sharing polynomial \(f\) to a secret \(s_0 = f\paren* 0\) with shares \(s_i = f\paren* i\). In Section \ref{sec.kem}, we adjust \cite{EPRINT:BDPV20}'s proving protocol to our setting of threshold schemes, so that knowledge of a subset of interpolation points for a modified sharing polynomial is proven instead of all interpolation points. 
%Let \(\mathcal R= \set{\left(x,f\right)}\) denote the set of statements \(x\) of the form \eqref{eq.pvprelation}, for which \(f\) is a witness polynomial. For a subset \(I \subset \set{1,\ldots, n}\), we denote by \(\mathcal R_I\) the set of partial relations
%\[\set{\left(x_I,f\right)\vert \exists x \colon \left(x,f\right) \in \mathcal R \wedge x_I = x\vert_I},\]
%where \(x\vert_I\) is the projection of \(x\) to the coordinates contained in \(I\).

%We amend the protocols presented in \cite{EPRINT:BDPV20}, which are tailored to a setting in which all shareholders are engaged, to fit our setting superauthorised sets of shareholders, which in general are significantly smaller.

%In this work, the prover is assumed to be a shareholder with index \(i\). Thus a PVP scheme consists of a proving and a verifying protocol. We denote the proving protocol by \(\mathsf{PVP}.P\), its input is the prover's index \(i\), the witness \(f\), a superauthorised set \(S'\) and the partial statement \(x_{S'}\). The verifying protocol \(\mathsf{PVP}.V\) has the prover's index \(i\), the verifyer's \(j\), the set \(S'\), the verifier's partial statement \(x_j\) and the proof piece \(\left(\pi,\pi_j\right)\) as input.  
%The proving and verifying protocols for superauthorised sets of shareholders are given in \autoref{fig.tpvpp} and \autoref{fig.tpvpv}, respectively. 

\subsection{Zero-Knowledge Proofs for the GAIP}
We define a proving and a verifying protocol to non-interactively prove knowledge of an element \(s\in\Z_p\) in zero-knowledge with respect to the group action inverse problem. That is a prover shows the knowledge of \(s\) so that
\[E'_i = \left[ s\right] E_i, \]
for \(E_i, E_i' \in \mathcal E\) and \(i=1,\ldots, m\), simultaneously, without revealing \(s\).

To prove the knowledge of \(s\), the prover samples \(b_j \in \Z_p\) and computes 
\[\hat{E}_{i,j} \gets \left[b_j\right] E_i\]
for \(i = 1, \ldots, m\) and \(j = 1, \ldots, \lambda\). He then derives challenge bits $$\left(c_1,\ldots, c_\lambda\right) \gets \mathcal H\paren*{E_1, E_1', \ldots, E_m, E_m', \hat E_{1,1} \ldots, \hat E_{m,\lambda}}$$ via a hash function \(\mathcal H : \mathcal E^{(2+\lambda)m}\to \set{0,1}^\lambda\) and prepares the answers \(r_j \gets b_j - c_j s\), \(j =1 ,\ldots,\lambda\). The proof \(\pi = \left(c_1, \ldots, c_\lambda, r_1, \ldots, r_\lambda\right)\) is then published.

The verification protocol is straight forward: for a statement \(\left(E_i,E'_i\right)_{i=1,\ldots,m}\) and a proof \(\pi = \left(c_1,\ldots, c_\lambda, r_1,\ldots, r_\lambda\right)\), the verifier computes \(\tilde E_{i,j}\gets \left[r_j\right] E_i\) if \(c_j = 0\) and \(\tilde E_{i,j} \gets \left[r_j\right] E'_i\) otherwise, for \(i = 1, \ldots, m\) and \( j = 1, \ldots, \lambda\). Then he generates verification bits \(\left(\tilde c_1 ,\ldots \tilde c_\lambda\right) \gets \mathcal H\paren*{E_1, E_1' , \ldots, E_m, E_m', \tilde E_{1,1} \ldots, \tilde E_{m,\lambda}}\) and accepts the proof if \(\left(c_1,\ldots, c_\lambda\right) = \left(\tilde c_1, \ldots, \tilde c_\lambda\right)\).

A sketch of the proving and verifying protocols can be found in \algoref{fig.zkp} and \algoref{fig.zkv}, respectively.

\begin{algorithm}
	\DontPrintSemicolon
	\SetAlgoShortEnd
	\SetAlgoVlined
	\SetInd{0.5em}{1em}
	\KwIn{$s, \left(E_i,E_i'\right)_{i=1,\ldots, m} $}
	\For{$j = 1,\ldots, \lambda$}{
		$b_j \sample \Z_p$\;
		\For{$i=1,\ldots, m$}{
			$\hat E_{ij} \gets \left[b_j\right]E_i$\;
		}
	}
	$\left(c_1,\ldots, c_\lambda\right) \gets \mathcal H\left(E_1, E_1', \ldots, E_m,E_m', \hat E_{1,1}, \ldots, \hat E_{m,\lambda}\right)$\;
	\For{$j = 1, \ldots, m$}{
		$r_j \gets b_j - c_j s$\;
	}
	\Return {$\pi \gets\left(c_1,\ldots, c_\lambda, r_1,\ldots, r_\lambda\right)$}
	\caption{The ZK proving protocol for the GAIP}
	\label{fig.zkp}
\end{algorithm}

\begin{algorithm}
	\DontPrintSemicolon
	\SetAlgoShortEnd
	\SetAlgoVlined
	\KwIn{$\pi, \left(E_i,E_i'\right)_{i=1,\ldots, m} $}
	Parse $\left(c_1,\ldots, c_\lambda,r_1,\ldots, r_\lambda\right) \gets \pi$\;
	\For{$i=1,\ldots, m \text{ and } j = 1,\ldots, \lambda$}{
		\If{$c_j == 0$}{
			$\tilde E_{i,j} \gets \left[r_j\right] E_i$\;
		}
        \Else{
            $\tilde E_{i,j} \gets \left[r_j\right] E_i'$\;
        }
	}
	$\left(c'_1,\ldots, c'_\lambda\right)\gets \mathcal H\paren*{E_1,E_1', \ldots, E_m,E_m',\tilde E_{1,1},\ldots, \tilde E_{m,\lambda}}$\;
	\Return {$\left(c_1,\ldots, c_\lambda\right) == \left(c'_1,\ldots, c'_\lambda\right)$}
	\SetInd{0.5em}{1em}
	\caption{The ZK verifying protocol for the GAIP}
	\label{fig.zkv}
\end{algorithm}

%The proving algorithm is presented in \autoref{fig.zkp} and the verifying algorithm can be found in \autoref{fig.zkv}. 
We again refer to \cite{EPRINT:BDPV20} for the proof of the presented algorithms being complete, sound and zero-knowledge with respect to the security parameter \(\lambda\).

\subsection{The Adversary}
	We consider a static and active adversary. At the beginning of a protocol execution, the adversary corrupts a set of shareholders. The adversary is able to see their inputs and control their outputs. The set of corrupted shareholders cannot be changed throughout the execution of the protocol.

	The adversary's aim is two-fold. On the one hand it wants to obtain information on the uncorrupted parties' inputs, on the other hand it wants to manipulate the execution of our protocol to return an incorrect output without detection.
	
\subsection{Communication channels}
Both our schemes assume the existence of a trusted dealer in addition to the shareholders engaged in a secret sharing instance.
The dealer samples and shares a private key and publishes the according public key. The shareholders store the private key and execute the multiparty protocols for decapsulation in our key exchange mechanism (KEM) and signing in our signature scheme.

For the communication between the dealer and the  shareholders we assume secure private channels. Thus messages sent through these channels cannot be tampered with or eavesdropped upon without detection. For the communication between the shareholders, however, a simple public channel is sufficient, since all messages sent by shareholders are being broadcast.
The means of how to establish secure private channels and immutable broadcast channels are out of scope of this work.
