\section{Generalising the secret sharing schemes}\label{sec.generalsss}

We constructed the protocols above in the context of Shamir's secret sharing protocol \cite{Shamir79}. The key exchange mechanism in \secref{sec.kem} as well as the signature scheme in \secref{sec.signatures} can be extended to more general secret sharing schemes. We first list the requirements that a secret sharing scheme has to meet in order to successfully implement the KEM and the signature scheme, then we give some examples of secret sharing schemes that fulfill said requirements.
\subsection{Compatibility requirements}
\begin{defin}[Independent Reconstruction]
	We say a secret sharing instance \(\mathcal S = \left(S, \Gamma, G\right)\) is \textbf{independently reconstructible}, if, for any shared secret \(s \in G\), any \(S'\in\Gamma\) and any shareholder \(P_i\in S'\), \(P_i\)'s input to reconstructing \(s\) is independent of the share of each other engaged shareholder \(P_j\in S'\).
\end{defin}
A secret sharing scheme compatible with our KEM and signature scheme has to have indenpendent reconstruction, since each shareholder's input into the threshold group action is hidden from every other party by virtue of the GAIP.\\
% This criterion is clearly met by Shamir's secret sharing.
\begin{defin}[Self-contained reconstruction]
	A secret sharing instance \(\mathcal S = \left(S,\Gamma,G\right)\) has \emph{self-contained reconstruction}, if, for any \(S'\in\Gamma\) and any share \(s\in G\), the input of each shareholder \(P_i \in S'\) to reconstructing \(s\) is in \(G\), so that reconstruction can be represented as an iterated application of the action of \(G\).
\end{defin}
%If the secret space is \(\Z_p\), this means that the shareholders' inputs are combined via addition.

For the secret space, \(G=\Z_p\) for some prime \(p\) is necessary to enable the mapping \(\cdot \mapsto [\cdot ]\). This requirement may be loosened by replacing \(\cdot \mapsto [\cdot]\) with an appropriate alternative. Also, for a secret \(s\) and any \(s_i \in \set{s_1,\ldots, s_k} \gets \mathcal S.\share\paren* s\), \(s_i \in G\) has to hold to enable key generation with two-level sharing.

Furthermore, a PVP scheme, that is compatible with the secret sharing scheme, has to exist and agree with a zero-knowledge proof with respect to the GAIP.

%And lastly, the access structure \(\Gamma\) for the secret sharing instance has to support superauthorised sets of shareholders. As we discussed in Section \ref{sec.kem}, this is necessary to ensure the soundness of the PVP.

\subsection{Examples of secret sharing schemes}

We give two examples of secret sharing schemes that fulfill the aforementioned conditions and two counter examples.

\begin{itemize}
	\item It is evident, that Shamir's approach fulfills all aforementioned requirements. We point out, that the two-level sharing and the PVP have been tailored to Shamir's polynomial based secret sharing approach.
	\item Tassa \cite{TCC:Tassa04} introduced a hierarchical secret sharing scheme also based on sharing via a randomly sampled polynomial. To share a secret \(s\), a polynomial \(f\) in \(\Z_p\left[X\right]\) is sampled with constant term \(s\). Shareholders of the top rank in the hierarchy are assigned interpolation points of \(f\). The second rank is assigned points on the first derivative, in short, shareholders of the \(k\)-th rank obtain interpolation points of the \(k-1\)st derivative. With the shares being in \(Z_p\), this enables the necessary two-level sharing. The polynomial based sharing approach agrees with the PVP protocol given above with some minor adjustments. Thus, transferring the KEM and the signature scheme to Tassa's secret sharing can easily be achieved.
	\item In 2006, Damgard and Thorbek proposed a linear integer secret sharing scheme \cite{PKC:DamTho06}. They enable a wide range of access structures by representing a given access strucure as a sharing matrix. For an integer secret \(s\) to be shared, a random vector with first entry \(s\) is sampled and multiplied with the sharing matrix. Thus reconstruction for an authorised set is achieved by inverting the corresponding submatrix corresponding and multiplying the set of their shares with the inverted matrix. With \(s\) and the shares being unbounded, Damgard's and Thorbek's scheme is not compatible with the mapping \(\cdot \mapsto [\cdot]\). Also their scheme does not comply with our PVP scheme. In its current form, our KEM and signature scheme cannot be instantiated with \cite{PKC:DamTho06}'s approach. If a suitable PVP and substitution for \(\cdot \mapsto [\cdot]\) can be found, an instantiation with their scheme is feasible.
	\item Additive secret sharing is the simplest of secret sharing schemes. For a given secret \(s\), each shareholder \(P_i\) receives a share \(s_i\), \(i =1 ,\ldots, n\), with \(s = \sum_{P_i} s_i\). Additive secret sharing has self-contained as well as independent reconstruction. Yet it is a full threshold secret sharing scheme, that is \(\Gamma = \set{S} = \set{\set{P_1,\ldots, P_n}}\). Thus for any \(P_i\in S\), the remaining shareholders \(\set{P_1,\ldots,P_n}\setminus \set{P_i}\) form an unauthorised set. Thus active security cannot be provided for the threshold group action, making additive secret sharing incompatible with our KEM and signature scheme.
\end{itemize}

