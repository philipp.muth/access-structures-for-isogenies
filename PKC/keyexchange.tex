\section{Key Exchange Mechanism}\label{sec.kem}

%\todo{argument for superauthorised sets}
We present our actively secure key exchange protocol with secret shared private key. A key exchange mechanism (KEM) provides three functions: \(\mathsf{KeyGen}\), \(\mathsf{Encaps}\) and \(\mathsf{Decaps}\).

\subsection{Public Parameters}
We fix the following public parameters.

\begin{itemize}
	\item A Shamir sharing instance $\mathcal S$ with shareholders $\mathcal S= \set{P_1, \ldots, P_n}$, threshold \(t< n\) and secret space \(\Z_p\). In \hyperref[sec.generalsss]{Section \ref{sec.generalsss}} we elaborate possible extensions to other, more general secret sharing schemes.

	\item A hard homogeneous space \(\left(\mathcal E, \mathcal G\right)\) with fixed starting point \(E_0 \in \mathcal E\).
    
	\item An element \(g \in\mathcal G\) with \(\mathsf{ord} g = p\) for the mapping \([\cdot] : \mathcal G \times \mathcal E \to \mathcal E; s \mapsto g^s E\).
\end{itemize}

\subsection{Key Generation}
We assume the existence of a trusted dealer, yet even an untrusted dealer can be accommodated with little overhead. For key generation, the dealer samples the secret key \(s \in \Z_p\) and publishes the public key \(\pk \gets [s] E_0\). The secret key \(s\) is shared via \(\left(s_1, \ldots, s_n\right)\gets \mathcal S.\share\paren* s\). Then, each share \(s_i\), \(1 \leq i \leq n\), is shared once more, resulting in \(n\) sets of \(n\) shares each.
\[\forall i = 1, \ldots, n \colon \set{s_{i1}, \ldots, s_{in}} \gets \mathcal S.\share\paren*{s_i}\]
The dealer then sends the following set to shareholder \(P_i\), \( i = 1, \ldots, n\):
\[\set{s_i, \set{s_{ij}}_{j=1,\ldots, n}, \set{s_{ki}}_{k = 1,\ldots, n}},\]
thus each shareholder \(P_i\) receives his share \(s_i\) of the secret key \(s\). He also receives the shares by which \(s_i\) was shared and his shares of each other shareholder's share of \(s\).

For ease of notation we denote the polynomial with which the secret key \(s\) was shared by \(f\) and the polynomial with which \(s_i\) was shared by \(f_i\), where \(i =1 , \ldots, n\).

This key generation protocol can be considered as a "two-level sharing", where each share of the secret key is itself shared again among the shareholders. A sketch of it can be found in \hyperref[fig.keygen]{Algorithm \ref{fig.keygen}}.
% \begin{figure}
% 	\procedure[space = auto]{$\mathsf{KeyGen}$}{
% 		s \sample \Z_p\\
% 		\pk \gets \left[ s\right] E_0\\
% 		\set{s_1,\ldots, s_n}\gets \mathcal S.\share\paren* s\\
% 		\pcfor i = 1, \ldots, n\\			
% 			\set{s_{i1}, \ldots, s_{in}} \gets \mathcal S.\share \paren*{s_i}\\
% 		\pcendfor\\
% 		\text{publish } \pk\\
% 		\pcfor i =1 ,\ldots, n\\
% 			\text{send }  \set{s_i, \set{s_{ij}}_{j=1,\ldots, n}, \set{s_{ki}}_{k = 1,\ldots, n}} \text{ to } P_i\\
% 		\pcendfor
% 	}
% 	\caption{The key generation protocol}
% 	\label{fig.keygen}
% \end{figure}

\begin{algorithm}
    \DontPrintSemicolon
    \SetAlgoShortEnd
    \SetAlgoVlined
    \SetInd{0.5em}{1em}
    %\KwIn{$i,j,S',x_j, \left(\pi,\pi_j\right)$}
    %\KwOut{a public message $M$}
    $s \sample \Z_p$\;
    $\assignTo{\pk}{\left[ s\right] E_0}$\;
    $\set{s_1,\ldots, s_n}\gets \mathcal S.\share\paren* s$\;
    \For{$i = 1, \ldots, n$}{
        $\set{s_{i1}, \ldots, s_{in}} \gets \mathcal S.\share \paren*{s_i}$\;
    }
    $\text{publish } \pk$\;
    \For{$i = 1, \ldots, n$}{
        $\text{send }  \set{s_i, \set{s_{ij}}_{j=1,\ldots, n}, \set{s_{ki}}_{k = 1,\ldots, n}} \text{ to } P_i$\;
    }    
    %\Return{$E^k$}
    \caption{Key generation}
    \label{fig.keygen}
\end{algorithm}


\subsection{Encapsulation}
With a public key \(\pk \in \mathcal E\) as input, the encapsulation protocol returns an ephemeral key \(\mathcal K\in \mathcal E\) and a ciphertext \(c\in \mathcal E\). 

Our encapsulation protocol is identical to the protocol of \cite{PKC:DeFMey20}, thus we just give a short sketch and refer to De Feo's and Meyer's work for the respective proofs of security.

% \begin{figure}
% \procedure[space=auto]{$\mathsf{Encaps} \paren* \pk$ }{
% 	b \sample \mathcal G\\
% 	\mathcal K\gets b \ast \pk\\
% 	c \gets b \ast E_0\\
% 	\pcreturn \left(\mathcal K,c\right)
% 	}
% 	\caption{The encapsulation protocol}
% 	\label{fig.encaps}
% \end{figure}


\begin{algorithm}
    \DontPrintSemicolon
    \SetAlgoShortEnd
    \SetAlgoVlined
    \SetInd{0.5em}{1em}
    \KwIn{$\pk$}
    %\KwOut{a public message $M$}
    $b \sample \mathcal G$\;
    $\assignTo{\mathcal K}{b \ast \pk}$\;
    $\assignTo{c}{b \ast E_0}$\;    
	\Return{$\left(\mathcal K,c\right)$}
    \caption{Encapsulation}
    \label{fig.encaps}
\end{algorithm}

\subsection{Decapsulation}
A decapsulation protocol takes a ciphertext \(c\) and outputs a key \(\mathcal K\).

De Feo and Meyer \cite{PKC:DeFMey20} applied the threshold group action (\hyperref[fig.tga]{Algorithm \ref{fig.tga}}) to a ciphertext \(c\) to have an authorised set of shareholders compute \(\left[s\right] c = \left[s\right] \left(b\ast E_0\right) = b \ast \left(\left[s\right] E_0\right)\), thereby decapsulating the ciphertext and obtaining the session key. While their approach is simulatable and thereby does not leak any information on the shares of the secret key, it is only passively secure. Thus, a malicious shareholder can provide malformed input to the protocol and thereby manipulate the output of the computation to incorrect results without being detected.

We extend their approach to enable detecting misbehaving shareholders in a decapsulation. For that we maintain the threshold group action, yet we apply the PVP and zero-knowledge proof layed out in \hyperref[sec.prelim]{Section \ref{sec.prelim}}.
Since the PVP does not fit our setting of threshold group action, we first discuss the necessary modifications to the PVP. We then present our actively secure decapsulation protocol.

\subsubsection{Amending the PVP}
In the PVP protocol, that we sketched in \hyperref[sec.prelim]{Section \ref{sec.prelim}}, a prover produces a proof of knowledge for a witness polynomial \(f\) of the statement
\[\left(\left(E_0,E_1\right),s_1, \ldots, s_n\right),\]
where \(E_0\sample \mathcal E\), \(E_1 = \left[s_0\right] E_0\) and \(s_i = f\paren* i\) for \(i = 0, \ldots, n\). He thereby proves knowledge of the sharing polynomial \(f\) of \(s_0 := f\paren* 0\). 

This does not fit the threshold group action, since, for an authorised set \(S'\), a shareholder \(P_i\)'s contribution to the round-robin approach is not \(E^k \gets \left[s_i\right]E^{k-1}\), where \(E^{k-1}\) denotes the previous shareholder's output, but \(E^k \gets \left[L_{i,S'} s_i \right] E^{k-1}\). Authorised sets also do not necessarily contain all shareholders \(\set{P_1, \ldots, P_n}\). The following example illustrates a further conflict with of the PVP with the threshold group action.

\begin{exm}
	Let \(\sk\) be a secret key generated and shared by \(\mathsf{KeyGen}\). That is each shareholder \(P_i\) holds
	\[\set{s_i, \set{s_{ij}}_{P_j \in S}, \set{s_{ji}}_{P_j \in S}}.\]
	Also let \(S'\in \Gamma\) be a minimally authorised set executing \hyperref[fig.tga]{Algorithm \ref{fig.tga}}, i.e., for any \(P_i \in S'\), \(S' \setminus \set{P_i}\) is unauthorised. Thus, for any arbitrary but fixed \(s_i'\in \Z_p\), there exists a polynomial \(f'_i\in \Z_p\left[X\right]_{k-1}\) so that \(f'_i \paren* j = L_{i,S'} s_{ij}\) and \(R' = \left[f'_i \paren* 0\right] R\) for any \(R,R' \in \mathcal E\). Thus, \(P_i\) can publish \(\left(\pi,\set{\pi_j}_{P_j \in S'}\right)\) with
	\begin{align*}
		\left(\pi,\set{\pi_j}_{P_j \in S}\right) \gets& \mathsf{PVP}.P\paren*{\left(\left(R,R'\right), \left(L_{i,S'} s_{ij}\right)_{P_j \in S}\right), f_i'}
	\end{align*}
	which to \(S'\setminus \set{P_i}\) is indistinguishable from 
	\[\mathsf{PVP}.P\paren*{\left(\left(E_0,E_1\right), \left(L_{i,S'} s_{ij}\right)_{P_j \in S}\right), L_{i,S'}f_i}\]
	with \(E_0 \sample \mathcal E\) and \(E_1 = \left[L_{i,S'} s_i\right] E_0\). Thus, for a minimally authorised set \(S'\), the soundness of the PVP does not hold with respect to \(P_i\in S'\) and \(f_i\).
\end{exm}

We resolve the conflicts by amending \cite{EPRINT:BDPV20}'s PVP protocol, so that, for a superauthorised set \(S^\ast\), a shareholder \(P_i \in S^\ast\) proves knowledge of a witness polynomial \(L_{i,S^\ast}f_i\) for a statement
\[\left(\left(R,R'\right),\left( L_{i,S^\ast} s_{ij}\right)_{P_j \in S^\ast}\right),\]
where \(R\sample\mathcal E\), \(R' = \left[L_{i,S^\ast} s_i\right] R\), \(s_{ij} = f_i\paren* j\) for \(P_j\in S^\ast\) and \(s_i\) was shared via \(f_i\).
The inputs of our amended proving protocol are the proving shareholder's index \(i\), the witness polynomial \(f\), the superauthorised set \(S^\ast \in\Gamma^+\) and the statement \(\left(\left(R,R'\right), \left( s_{ij}\right)_{P_j\in S^\ast}\right)\). The protocol can be found in \hyperref[fig.tpvpp]{Algorithm \ref{fig.tpvpp}}. By \(\mathcal C\) we denote a commitment scheme.
The verifying protocol in turn has the prover's and the verifier's indices \(i\) and \(j\), respectively, a set \(S^\ast\in\Gamma^+\), a statement piece \(x_j\) and a proof piece \(\left(\pi,\pi_j\right)\) as input, where \(x_j = \left(R,R'\right)\in\mathcal E^2\) if \(j=0\) and \(x_j\in\Z_p\) otherwise. The verifying protocol is given in \hyperref[fig.tpvpv]{Algorithm \ref{fig.tpvpv}}.

The definitions of soundness and zero-knowledge for a threshold PVP scheme carry over from the non-threshold setting in \hyperref[sec.prelim]{Section \ref{sec.prelim}} intuitively, yet we restate the completeness definition for the threshold setting.
\begin{defin}[Completeness in the threshold setting]
	We call a threshold PVP scheme \emph{complete} if, for any \(S'\in \Gamma\), any \(\left(x,f\right)\in\mathcal R\), any \(P_i \in S'\) and \(\left(\pi,\set{\pi_j}_{P_j \in S'}\right) \gets \mathsf{PVP}.P\paren*{i,f,S',x_{S'}}\), we have
	\[\prob{\mathsf{PVP}.V \paren*{i,j,S', x_j, \left(\pi,\pi_j\right)} = \true} = 1\]
	for all \(P_j \in S'\).
\end{defin}
The proofs for soundness, correctness and zero-knowledge for Beullens et al.'s \cite{EPRINT:BDPV20} approach are easily transferred to our amended protocols, thus we do not restate them here. 
% \begin{figure}
% 	\procedure[space= auto]{$\mathsf{PVP}.P\paren*{i, f, S', \left(\left(E_0,E_1\right), \left( s_{ij}\right)_{P_j \in S', }\right)}$}{
% 			%
% 		\pcfor l = 1 ,\ldots, \lambda \\
% 			b_l \sample \Z_N [x]_{\leq k-1}\\
% 			\hat{E}_l \gets \left[ b_l\paren* 0\right] E_0\\
% 		\pcendfor\\
% 		y_0, y_0' \sample \set{0,1}^\lambda\\
% 		C_0 \gets \mathcal C\paren*{\hat{E}_1 \concat \ldots \concat \hat{E}_\lambda, y_0}\\
% 		C_0' \gets \mathcal C \paren*{E_0\concat E_1, y_0'}\\
% 		\pcfor P_j \in S'\\
% 			y_j, y_j' \sample \set{0,1}^\lambda\\
% 			C_j \gets \mathcal C\paren*{b_1\paren* j \concat \ldots\concat b_\lambda\paren*j, y_j}\\
% 			C_j'\gets \mathcal C\paren*{L_{i,S'} \cdot s_{ij}, y_j'}\\
% 		\pcendfor\\
% 		C \gets \left(C_j\right)_{P_j \in S'}\\
% 		C'\gets \left(C_j'\right)_{P_j \in S'}\\
% 		c_1, \ldots, c_\lambda \gets \mathcal H\paren*{C,C'}\\
% 		\pcfor l = 1, \ldots, \lambda\\
% 			r_l \gets b_l - c_l \cdot L_{i,S'}\cdot f\\
% 		\pcendfor\\
% 		\mathbf{r} \gets \left(r_1, \ldots, r_\lambda\right)\\
% 		\left(\pi,\set{\pi_j}_{P_j \in S'}\right) \gets\left( \left(C,C',\mathbf{r}\right), \set{ \left( y_j,y_j'\right)}_{P_j \in S'}\right)\\
% 		\pcreturn \left(\pi,\set{\pi_j}_{P_j \in S'}\right) 
% 	}
% 	\caption{The proving protocol of the threshold PVP}
% 	\label{fig.tpvppOLD}
% \end{figure}


\begin{algorithm}
    \DontPrintSemicolon
    \SetAlgoShortEnd
    \SetAlgoVlined
    \SetInd{0.5em}{1em}
    \KwIn{$i, f, S^\ast, ((E_0,E_1), (s_{ij})_{P_j \in S^\ast})$}
    %\KwOut{a public message $M$}
    \For{$l \in 1, \ldots, \lambda$}{
        $b_l \sample \Z_N [x]_{\leq k-1}$\;
        $\hat{E}_l \gets \left[ b_l\paren* 0\right] E_0$\;
    }
    $y_0, y_0' \sample \set{0,1}^\lambda$\;
    $C_0 \gets \mathcal C\paren*{\hat{E}_1 \concat \ldots \concat \hat{E}_\lambda, y_0}$\;
    $C_0' \gets \mathcal C \paren*{E_0\concat E_1, y_0'}$\;
    \For{$P_j \in S^\ast$}{
        $y_j, y_j' \sample \set{0,1}^\lambda$\;
        $C_j \gets \mathcal C\paren*{b_1\paren* j \concat \ldots\concat b_\lambda\paren*j, y_j}$\;
        $C_j'\gets \mathcal C\paren*{L_{i,S^\ast} \cdot s_{ij}, y_j'}$\;
    } 
    $C \gets \left(C_j\right)_{P_j \in S^\ast}$\;
    $C'\gets \left(C_j'\right)_{P_j \in S^\ast}$\;
    $c_1, \ldots, c_\lambda \gets \mathcal H\paren*{C,C'}$\;
    \For{$l \in 1, \ldots, \lambda$}{
        $r_l \gets b_l - c_l \cdot L_{i,S^\ast}\cdot f$\;
    }
    $\mathbf{r} \gets \left(r_1, \ldots, r_\lambda\right)$\;
    $\left(\pi,\set{\pi_j}_{P_j \in S^\ast}\right) \gets\left( \left(C,C',\mathbf{r}\right), \set{ \left( y_j,y_j'\right)}_{P_j \in S^\ast}\right)$\;
    \Return{$\left(\pi,\set{\pi_j}_{P_j \in S^\ast}\right)$}
    \caption{Proving protocol of the threshold PVP}
    \label{fig.tpvpp}
\end{algorithm}



% \begin{figure}
% 	\procedure[space = auto]{$\mathsf{PVP}.V\paren*{i,j,S',x_j, \left(\pi,\pi_j\right)}$}{
% 		\text{parse } \left(C,C',\mathbf r\right) \gets \pi\\
% 		\text{parse } \left(y_j, y_j'\right) \gets \pi_j\\
% 		c_1, \ldots, c_\lambda \gets \mathcal H \paren*{C,C'}\\
% 		%
% 		\pcif j == 0 \\%\pccomment{the case of \(x_j = \left(E_0,E_1\right)\)}
% 			\pcif C'_j \neq \mathcal C \paren*{ x_j , y'_j} \\
% 				\pcreturn \false\\
% 			\pcfi\\
% 			\pcfor l = 1, \ldots, \lambda\\
% 				\tilde{E}_l \gets \left[ r_l\paren*0\right] E_{c_l}\\
% 			\pcendfor\\
% 			\pcreturn C_0 == \mathcal C \paren*{\tilde{E}_1 \concat \ldots \concat \tilde{E}_\lambda, y_0}\\
% 		\pcelse\\%\pccomment{the case of \(x_j \in \Z_N\)}\\
% 			\pcif C'_j \neq \mathcal C \paren*{L_{i,S'} x_j , y'_j} \\
% 				\pcreturn \false\\
% 			\pcfi\\
% 			\pcreturn C_j == \mathcal C (r_1 \paren* j + \\\hspace{70pt}c_1 \cdot L_{i,S'}\cdot x_j \concat \ldots\concat r_\lambda\paren* j + \\\hspace{70pt}c_\lambda \cdot L_{i,S'}\cdot x_j,y_j)\\
% 		\pcfi
% 	}
% 	\caption{The verifying protocol of the threshold PVP}
% 	\label{fig.tpvpv}
% \end{figure}

\begin{algorithm}
    \DontPrintSemicolon
    \SetAlgoShortEnd
    \SetAlgoVlined
    \SetInd{0.5em}{1em}
    \KwIn{$i,j,S^\ast,x_j, (\pi,\pi_j)$}
    $\text{parse } (C,C',\mathbf r) \gets \pi$\;
    $\text{parse } (y_j, y_j') \gets \pi_j$\;
    $c_1, \ldots, c_\lambda \gets \mathcal H (C,C')$\;
    \If{$j == 0$}{
        \If{$C'_j \neq \mathcal C (x_j , y'_j)$}{
            \Return{$\false$}
        }
        \For{$l \in 1, \ldots, \lambda$}{
            $\tilde{E}_l \gets \left[ r_l\paren*0\right] E_{c_l}$\;
        }
        \Return{$C_0 == \mathcal C\paren*{\tilde{E}_1 \concat \ldots \concat \tilde{E}_\lambda, y_0}$}
    }
        \Else{
            \If{$C'_j \neq \mathcal C \paren*{L_{i,S^\ast} x_j , y'_j}$}{
                \Return{$\false$}
            }
            \Return{$C_j == \mathcal C(r_1 \paren* j + c_1 \cdot L_{i,S^\ast}\cdot x_j \concat \ldots\concat r_\lambda\paren* j + c_\lambda \cdot L_{i,S^\ast}\cdot x_j,y_j)$}
        }
    \caption{Verifying protocol of the threshold PVP}
    \label{fig.tpvpv}
\end{algorithm}


We arrive at our decapsulation protocol, executed by a superauthorised set \(S^\ast\): The partaking shareholders fix a turn order. A shareholder \(P_i\)'s turn consists of the following steps.
\begin{enumerate}
	\item If the previous shareholder's output \(E^{k-1}\) is not in \(\mathcal E\), \(P_i\) outputs \(\bot\) and aborts. The first shareholder's input \(E^0\) is the protocol's input ciphertext \(c\).
	\item Otherwise \(P_i\) samples \(R_k \sample \mathcal E\) and computes \(R_k' \gets \left[L_{i,S'} s_i\right]R_k\).
	\item\label{step.pvp} \(P_i\) computes and publishes $$\left(\pi^k,\set{\pi^k_j}_{P_j\in S^\ast}\right) \gets \mathsf{PVP}.P \paren*{i,f_i,S^\ast,\left(\left(R_k,R_k'\right), \left(s_{ij}\right)_{P_j\in S^\ast}\right)}.$$
	\item Each shareholder \(P_j \in S^\ast\setminus\set{P_i}\) verifies $$\mathsf{PVP}.V \paren*{i, j,S^\ast,s_{ij},\left(\pi^k,\pi^k_j\right)} \text{ and } \mathsf{PVP}.V\paren*{i,0,S^\ast,\left(R_k,R_k'\right),\left(\pi^k,\pi_0^k\right)}.$$ If either should fail, \(P_j\) issues a complaint against \(P_i\) and publishes \(s_{ij}\). If \(P_i\) is convicted of cheating by more than \(\nicefrac{\# S^\ast}{2}\) shareholders, decapsulation is restarted with an \({S^\ast}'\in \Gamma^+\), so that \(P_i \not \in {S^\ast}'\).
	\item\label{step.zk} If the PVP was accepted, \(P_i\) computes \(E^k \gets \left[L_{i,S^\ast} s_i\right] E^{k-1}\) as well as the zero-knowledge proof \(zk \gets \mathsf{ZK}.P \paren*{\left(R_k,R_k'\right), \left(E^{k-1}, E^k\right), L_{i,S^\ast} s_i}\). He publishes both.
	\item If \(\mathsf{ZK}.V\paren*{\left(R_k, R_k'\right), \left(E^{k-1},E^k\right),zk}\) fails to verify, decapsulation is restarted with a set \({S^\ast}'\in\Gamma^+\), where \(P_i\not\in{S^\ast}'\).
	\item Otherwise, \(P_i\)'s turn is finalised and the next shareholder continues with \(E^k\) as input from \(P_i\).
	\item The protocol terminates  with the last shareholder's \(E^{\# S^\ast}\) as output.
\end{enumerate}
The combination of the PVP and the zero-knowledge proof in steps \ref{step.pvp} and \ref{step.zk} ensure, that \(P_i\) not only has knowledge of the sharing polynomial \(L_{i,S^\ast} f_i\) but also inputs \(L_{i,S^\ast} f_i\paren* 0\) to compute \(E^k\). The precise protocol can be found in \hyperref[fig.decaps]{Algorithm \ref{fig.decaps}}.

% \begin{figure}
% %\begin{tiny}
% 	\procedure[space = auto]{$\mathsf{Decaps} \paren*{c, S^\ast}$}{
% 		E^0 \gets c; k \gets 0\\
% 		\pcfor P_i \in S^\ast\\
% 			\pcif E^k \not \in\mathcal E\\
% 				P_i \text{ outputs } \bot \text{ and aborts.}\\
% 			\pcfi\\
% 			k \gets k+1; R_k \sample \mathcal E; R_k' \gets \left[L_{i,S^\ast} s_i\right]R_k\\
% 			\left(\pi^k,\set{\pi^k_j}_{P_j \in S^\ast}\right) \gets \mathsf{PVP}.P(i,f_i,\\\hspace{120pt}S^\ast,((R_k,R_k'),\\\hspace{120pt}(L_{i,S^\ast}s_{ij})_{P_j\in S^\ast}))\\
% 			P_i \text{ publishes } \left(R_k,R_k'\right) \text{ and }\left(\pi^k,\set{\pi^k_j}_{P_j \in S^\ast}\right)\\
% 			\text{Each } P_j \in S^\ast \text{ checks }\\
% 			\pcind b_j \gets \mathsf{PVP}.V\paren*{i, j,S^\ast,L_{i,S^\ast} s_{ij},\left(\pi^k,\pi^k_j\right)} \\
% 			\pcind[2] \wedge \mathsf{PVP}.V\paren*{i, 0,S^\ast, \left(R_k, R_k'\right),\left(\pi^k,\pi^k_0\right)}\\
% 			\pcif b_j = \false \text{ for some } P_j\\
% 				P_j \text{ publishes } s_{ij}\\
% 				\pcif P_i \text{ is convicted}\\
% 					\pcreturn \mathsf{Decaps}\paren*{c,{S^\ast}'} \text{ with } {S^\ast}' \in \Gamma \wedge P_i \not \in {S^\ast}'\\
% 				\pcfi\\
% 			\pcfi\\
% 			E^k \gets \left[L_{i,S^\ast} s_i \right]E^{k-1}\\
% 			zk^k \gets \mathsf{ZK}.P\paren*{\left(R_k,R'_k\right),\left(E^{k-1},E^k\right), L_{i,S^\ast} s_i}\\
% 			P_i \text{ publishes } \left(E^k, zk\right)\\
% 			\text{Each } P_j \in S^\ast \text{ checks }\\
% 			\pcif \mathsf{ZK}.V\paren*{\left(R_k,R_k'\right),\left(E^{k-1},E^k\right),zk} = \false\\
% 				\pcreturn \mathsf{Decaps}\paren*{c,{S^\ast}'} \text{ with } {S^\ast}' \in \Gamma \wedge P_i \not \in {S^\ast}'\\
% 			\pcfi\\
% 		\pcendfor\\
% 		\pcreturn \mathcal K \gets E^k
% 		}
% 	\caption{The decapsulation protocol}
% 	\label{fig.decaps}
% %\end{tiny}
% \end{figure}


\begin{algorithm}[t]
    \DontPrintSemicolon
    \SetAlgoShortEnd
    \SetAlgoVlined
    \SetInd{0.5em}{1em}
    \KwIn{$c, S^\ast$}
    $E^0 \gets c$\;
    $k \gets 0$\;
    \For{$P_i \in S^\ast$}{
        \If{$E^k \not \in\mathcal E$}{
            $P_i \text{ outputs } \bot \text{ and aborts.}$\;
        }
        $k \gets k+1$\; 
        $R_k \sample \mathcal E$\; 
        $R_k' \gets \left[L_{i,S^\ast} s_i\right]R_k$\;
        $\left(\pi^k,\set{\pi^k_j}_{P_j \in S^\ast}\right) \gets \mathsf{PVP}.P\left(i,f_i,S^\ast,((R_k,R_k'),(L_{i,S^\ast}s_{ij})_{P_j\in S^\ast})\right)$\;
        $P_i \text{ publishes } \left(R_k,R_k'\right) \text{ and }\left(\pi^k,\set{\pi^k_j}_{P_j \in S^\ast}\right)$\;
	$\text{Each } P_j \in S^\ast\setminus\set{P_i} \text{ checks }$\;
        $b_j \gets \mathsf{PVP}.V\paren*{i, j,S^\ast,L_{i,S^\ast} s_{ij},\left(\pi^k,\pi^k_j\right)}$\;
        \If{$b_j = \false \text{ for some } P_j$}{
            $P_j \text{ publishes } s_{ij}$\;
            \If{$ P_i \text{ is convicted}$}{
                \Return{$\mathsf{Decapsulation}\paren*{c,{S^\ast}'} \text{ with } {S^\ast}' \in \Gamma \wedge P_i \not \in {S^\ast}'$}
            }
        }
        $E^k \gets \left[L_{i,S^\ast} s_i \right]E^{k-1}$\;
        $zk^k \gets \mathsf{ZK}.P\paren*{\left(R_k,R'_k\right),\left(E^{k-1},E^k\right), L_{i,S^\ast} s_i}$\;
        $P_i \text{ publishes } \left(E^k, zk\right)$\;
	$\text{Each } P_j \in S^\ast\setminus\set{P_i} \text{ checks }$\;  
        \If{$\mathsf{ZK}.V\paren*{\left(R_k,R_k'\right),\left(E^{k-1},E^k\right),zk} = \false$}{
            \Return{$\mathsf{Decapsulation}\paren*{c,{S^\ast}'} \text{ with } {S^\ast}' \in \Gamma \wedge P_i \not \in {S^\ast}'$}
        }                 

        }
        \Return{$\mathcal K \gets E^k$}

    \caption{Decapsulation}
    \label{fig.decaps}
\end{algorithm}



\begin{defin}
A KEM with secret shared private key is \textbf{correct}, if for any authorised set \(S'\), any public key \(\pk\) and any \(\left(\mathcal K,c\right)\gets \mathsf{Encaps}\paren* \pk\), we have
\[ \mathcal K = \mathcal K' \gets \mathsf{Decaps}\paren*{c,S'}.\]
\end{defin}
The correctness of our KEM presented in \hyperref[fig.keygen]{Algorithm \ref{fig.keygen}}, \hyperref[fig.encaps]{Algorithm \ref{fig.encaps}} and \hyperref[fig.decaps]{Algorithm \ref{fig.decaps}} follows from the correctness of the threshold group action (\hyperref[fig.tga]{Algorithm \ref{fig.tga}}). Let \(\pk\) be a public key and \(\sk = \left[\pk\right] E_0\) be the respective secret key, that have been generated by \(\mathsf{KeyGen}\), thus each shareholder \(P_i\) holds a share \(s_i\) of \(\sk\), \(i=1,\ldots, n\). For an authorised set \(S'\) we therefore have
\[\sk = \sum_{P_i \in S^\ast} L_{i,S^\ast} s_i.\]
Furthermore let \(\left(\mathcal K,c\right) \gets \mathsf{Encaps}\paren* \pk\). To show correctness, \(\mathcal K' = \mathcal K\) has to hold, where \(\mathcal K' \gets \mathsf{Decaps}\paren*{c,S'}\). Now, after executing \(\mathsf{Decaps}\paren*{c,S'}\), we have \(\mathcal K' = E^{\#S'}\), which emerges as a result of applying the threshold group action to \(c\). This gives us
\[ \mathcal K' = \left[\sum_{P_i \in S'} L_{i,S'}s_i \right] c = \left[\sk\right] \left(b\ast E_0\right) = b \ast \pk = \mathcal K.\]
Our decapsulation is executed by superauthorised sets \(S^\ast\), which are authorised. This shows that our KEM is correct.

\subsection{Security}

There are two aspects of security to consider:
\begin{itemize}

	\item Active security: A malicious shareholder cannot generate his contribution to the decapsulation protocol dishonestly without being detected and identified. We prove this by showing that an adversary that can provide malformed inputs without detection can break either the PVP or the zero-knowledge proof of knowledge.

	\item Simulatability: An adversary that corrupts an unauthorised set of shareholders cannot learn any information about the uncorrupted shareholders' inputs from an execution of the decapsulation protocol. We show this by proving the simulatability of \(\mathsf{Decaps}\).
\end{itemize}

\subsubsection{Active security}
\begin{thm}
	Let \(S^\ast \in \Gamma^+\) and let \(\left(\pk,\sk\right) \gets \mathsf{KeyGen}\) be a public/secret key pair, where \(\sk\) has been shared. Also let \(\left(\mathcal K,c\right) \gets \mathsf{Encaps}\paren*{\pk}\). Denote the transcript of \(\mathsf{Decaps}\paren*{c,S^\ast}\) by
	\[\left(E^k,\left(R_k, R_k'\right),\left(\pi^k,\set{\pi^k_j}_{P_j\in S^\ast}\right), zk_k\right)_{k=1,\ldots, \#S^\ast} .\]
	Let \(P_i \in S^\ast\) be an arbitrary but fixed shareholder. If \(\mathsf{Decaps}\paren*{c,S^\ast}\) terminated  successfully and \(P_{i'}\)'s output was generated dishonestly, then there exists an algorithm that breaks the soundness property of \(\mathsf{PVP}\) or \(\mathsf{ZK}\).
\label{thm.actsecu}\end{thm}

\begin{proof}
	Let \(P_{i'}\) be the malicious shareholder and let \(k'\) be the index of \(P_{i'}\)'s output in the transcript. Since \(\mathsf{Decaps}\paren*{c,S^\ast}\) terminated successfully, we have 
	\begin{align}
		%\mathsf{PVP}.V\paren*{i', j,S^\ast, L_{i',S^\ast} s_{i'j}, \left(\pi^{k'},\pi^{k'}_j\right)} =& \true\label{eq.pvp1}\\
		\mathsf{PVP}.V\paren*{i', j,S^\ast, s_{i'j}, \left(\pi^{k'},\pi^{k'}_j\right)} =& \true\label{eq.pvp1}\\
		\mathsf{PVP}.V\paren*{i', 0,S^\ast, \left(R_{k'},R_{k'}' \right), \left(\pi^{k'},\pi^{k'}_0\right)} =& \true\label{eq.pvp2}\\
		\mathsf{ZK}.V\paren*{\left(E^{k'-1},E^{k'}\right), \left(R_{k'}, R_{k'}'\right), zk^{k'}} =& \true \label{eq.zk}
	\end{align}
	\todo{changed \eqref{eq.pvp1}}
	for all \(P_j \in S^\ast\setminus\set{P_{i'}}\). \(E^{k'}\) was generated dishonestly, thus we have 
	\[ E^{k'} = \left[\alpha\right] E^{k'-1},\]
	for some \(\alpha \neq L_{i',S^\ast} s_{i'}\). We distinguish two cases: \(R_{k'}' \neq \left[\alpha\right] R_{k'}\) and \(R_{k'}' = \left[\alpha\right] R_{k'}\). 
	
	In the first case, \(P_{i'}\) published a zero-knowledge proof \(zk^{k'}\) so that \eqref{eq.zk} holds, where \(E^{k'} = \left[\alpha\right] E^{k'-1}\) yet \(R_{k'}' \neq \left[\alpha\right] R_{k'}\). \(P_{i'}\) thus broke the soundness property of the zero-knowledge proof.

	In the second case, \(P_{i'}\) published \(\left(\pi^{k'},\set{\pi_{j}^{k'}}_{P_j \in S^\ast}\right)\) so that \eqref{eq.pvp1} and \eqref{eq.pvp2} hold for all \(P_j \in S^\ast\setminus \set{P_{i'}}\) and for \(j=0\). Thus, \(P_{i'}\) proved knowledge of a witness polynomial \(f'\) with
	\begin{equation}
		f'\paren* j = L_{i',S^\ast} s_{ij}
	\label{eq.interpol}\end{equation}
	for all \(P_j \in S^\ast\setminus\set{P_{i'}}\) and \( R_{k'}' = \left[f'\paren* 0\right] R_{k'}\), that is
	\[f'\paren* 0 = \alpha.\]
	Since \(f'\) has degree at most \(k-1\), it is well-defined from  \eqref{eq.interpol}. Thus we have \(f' \equiv L_{i',S^\ast} f_{i'}\), where \(f_{i'}\) is the polynomial with which \(s_i\) was shared, i.e., \(f_i \paren* 0 = s_i\). This gives us \(\alpha = f' \paren* 0 = L_{i',S^\ast} f_{i'} \paren* 0 = L_{i',S^\ast} s_{i'j}\). We arrive at a contradiction, assuming the soundness of the PVP.
\end{proof}

\subsubsection{Simulatability}
We show, that an adversary who corrupts an unauthorised subset of shareholder does not learn any additional information from an execution of the decapsulation protocol. For that we prove the simulatability of the decapsulation.


\begin{thm}
	The decapsulation protocol presented in \hyperref[fig.decaps]{Algorithm \ref{fig.decaps}} is simulatable.
\end{thm}
%In short if an \(\sskem\) is simulatable, an adversary cannot derive any meaningful information concerning the secret key from the transcript of an execution of the decapsulation algorithm, since with just the information he gathered from partaking in the decapsulation execution, a transcript can be generated that is indistinguishable from the actual transcript.
\begin{proof}
We give a finite series of simulators, the first of which simulates the behaviour of the uncorrupted parties faithfully and the last of  which fulfills the secrecy requirements. This series is inspired by the simulators, that \cite{EPRINT:BDPV20} gave for the secrecy proof of their key generation algorithm, yet differs in some significant aspects. The outputs of the respective simulators will be proven indistinguishable, hence resulting in the indistinguishability of the first and last one. As a slight misuse of the notation, we denote the set of corrupted shareholders by \(\adv\), where \(\adv\) is the adversary corrupting an unauthorised set of shareholders. This means \(P_i\) is corrupted iff \(P_i \in \adv\).
	%\todo{sim1 is not actually faithful, but indistinguishable}

The input for each simulator is a ciphertext \(c\), a derived key \(\mathcal K\) and the adversary's knowledge after \(\mathsf{KeyGen}\) was successfully executed, that is
\[\set{s_i, \set{s_{ij}}_{P_i \in S^\ast},\set{s_{ji}}_{P_j \in S^\ast\setminus \adv}}_{P_i\in \adv}.\]
\begin{enumerate}
	\item The adversary corrupted an unauthorised set \(\adv\), hence each share of the secret key is uniformly distributed from his view. \(\simul^1\) samples a polynomial \(f'_i \in \Z_p\left[X\right]_{k-1}\) with 
		\[\forall P_j \in \adv: f'_i \paren* i = s_{ij}\]
		uniformly at random for each \(P_i \in S^\ast \setminus \adv\). Since \(\adv\) is unauthorised, \(f'_i\) exists.

		\(\simul^1\) then proceeds by honestly producing the output of each \(P_i \in S^\ast\setminus \adv\) according to the decapsulation protocol, i.e., it samples \(R_k \sample \mathcal E\), computes \(R_k' \gets \left[L_{i,S^\ast} f_i'\paren* 0\right] R_k\) and outputs \(\mathsf{PVP}.P\paren*{i,f_i',S^\ast,\left(\left(R_k,R_k'\right),\left(L_{i,S^\ast}s_{ij}\right)_{P_j \in S^\ast}\right)}\), \(E^k \gets \left[L_{i,S^\ast} s'_i\right] E^{k-1}\) and \(\mathsf{ZK}.P\paren*{\left(R_k, R_k'\right),\left(E^{k-1},E^k\right),L_{i,S^\ast}f'_i\paren* 0}\), where \(k\) is the index of \(P_i\)'s output in the transcript, \(s_{ij}: = f_i'\paren* j\) for \(P_j \in S^\ast \setminus \adv\) and \(s'_i := f_i'\paren* 0\). Since, for all \(P_i \in S^\ast\setminus\adv\), \(s_i\) is information theoretically hidden to the adversary, the resulting transcript is identically distributed to a real transcript.

	\item Let \(i'\) denote the index of the last honest party in the execution of the decapsulation protocol and \(k'\) the index of its output. \(\simul^2\) behaves exactly as \(\simul^1\) with the exception, that it does not compute the PVP itself but calls the simulator \(\simul^\mathsf{PVP}\) for the PVP to generate the proof \(\left(\pi^{k'},\set{\pi^{k'}_j}\right)\) for the statement \(\left(\left(R_{k'},R_{k'}'\right), \left(L_{i,S^\ast} s_{i'j}\right)_{P_j \in S^\ast}\right)\). Since the PVP is zero-knowledge, \(\simul^2\)'s output is indistinguishable from that of \(\simul^1\).

	\item \(\simul^3\) behaves identical to \(\simul^2\) apart from not generating the zero-knowledge proof for \(P_{i'}\) itself, but outsourcing it to the simulator for the zero-knowledge proof. That is \(\simul^3\) hands tuples \(\left(R_{k'}, R_{k'}'\right)\) and \(\left(E^{k'-1}, E^{k'}\right)\) to \(\simul^\mathsf{ZK}\) and publishes its answer as the zero-knowledge proof. With \(\mathsf{ZK}\) being zero-knowledge, the output of \(\simul^3\) is indistinguishable from that of \(\simul^2\).

	\item The final simulator, \(\simul^4\), enforces the correct decapsulation output, that is \(E^{\# S^\ast} = \mathcal K\). Since, for \(P_j \in \adv\), \(s_j\) was provided as input and \(P_{i'}\) is the last honest shareholder in the order of decapsulation execution, \(\simul^4\) computes
		\[ \sum_{P_j \in S'} L_{j,S^\ast}s_j,\]
		where \(S'\) contains the shareholders, whose turn is after \(P_{i'}\)'s. To achieve the correct output of the decapsulation \(E\), \(\simul^4\) thus sets 
		\[E^{k'} \gets \left[-\sum_{P_j \in S' } L_{j,S^\ast} s_j \right] E\]
		instead of \(E^{k'} \gets \left[L_{i',S^\ast} s'_{i'}\right]E^{k' -1}\). Assuming the soundness of the PVP as well as of the zero-knowledge proof, this guarantees the result to be \(E^{\# S^\ast} = E\), since
		\[E^{\# S^\ast} = \left[\sum_{P_j \in S'} L_{j,S^\ast}s_j\right] E^{k'} = E\]
		holds. It remains to show, that the output of \(\simul^4\) cannot be distinguished from that of \(\simul^3\). The following reasoning is similar to that of \cite{EPRINT:BDPV20}, yet for completeness we give a reduction \(\bdv'\), that uses a distinguisher \(\adv'\), that distinguishes \(\simul^3\) from \(\simul^4\), to break the decisional parallelisation problem. We highlight the necessary modifications.
		
		Let \(\left(E_a, E_b,E_c\right)\) be an instance of the decisional parallelisation problem with base element \(c\). \(\bdv'\) computes 
		\[E^{k'} \gets \left[ \sum_{P_j \in S^\ast\setminus \left(S' \cup \set{P_{i'}}\right)} L_{j, S^\ast} s_j \right] E_a.\]
		With \(s_{i'}\) looking uniformly distributed from \(\adv\)'s view, this choice of \(E^{k'}\) is indistinguishable from \(E^{k'} = \left[L_{i',S^\ast}s'_{i'}\right] E^{k'-1}\).
		\(\bdv'\) furthermore does not sample \(R_{k'} \sample \mathcal E\) but puts \(R_{k'} \gets E_b\) and \(R_{k'}' \gets E_c\). The resulting transcript is handed to \(\adv'\) and \(\bdv'\) outputs whatever \(\adv'\) outputs.
	
		Comparing the distributions, we see that
		\begin{align*}
			E^{k'} &= \left[a\right] E^{k'-1} \\
			&= [a] \left(\left[\sum_{P_j \in S^\ast\setminus \left(S' \cup \set{P_{i'}}\right)} L_{j, S^\ast} s_j\right] c\right)
		\end{align*}
		if and only if \(E_a = [a] c\), where \(s_j := s'_j\) for \(P_j \not \in \adv\). Furthermore,
		\[R'_{k'}  = [a] R_{k'}\]
		is equivalent to
		\[E_c = [a] E_b.\]
		In the case of \(E_a = [a] c\) and \(E_c = [a]E_b\), the transcript handed to \(\adv'\) is identically distributed to \(\simul^3\)'s output. If, on the other hand, \(\left(E_a,E_b,E_c\right)\) is a random triple, then the transcript follows the same distribution as \(\simul^4\)'s output. \(\bdv'\) thus breaks the DPP with the same advantage as \(\adv'\) distinguishes \(\simul^3\) from \(\simul^4\).
\end{enumerate}

	\(\simul^4\) outputs a transcript of the decapsulation protocol with input \(c\) and output \(\mathcal K\) that cannot be distinguished from the output of \(\simul^1\), which is indistinguishable from a real execution protocol.
\end{proof}

\subsection{Efficiency}
Each shareholder engaged in an execution of the decapsulation protocol has one round of messages to send. The messages of the \(k\)-th shareholder consist of the tuple \(\left(R_k,R_k'\right)\), a PVP proof \(\left(\pi^k,\set{\pi^k_j}_{P_j\in S^\ast}\right)\), the output \(E^k\) and the zero-knowledge proof \(zk\). Thus the total size of a shareholder's messages is
\begin{align*}
	&2 x + 2c + \lambda k \log p + 2\lambda (\#S^\ast) + x + \lambda k \log p + \lambda\\
	=& 3x + 2c + \lambda \left(1 + 2(\#S^\ast) + 2 k \log p\right)
\end{align*}
where \(x\) is the bit representation of an element of \(\mathcal E\) and \(c\) is the size of a commitment produced in \(\mathsf{PVP}.P\). Assuming \(x\), \(c\) and the secret sharing parameters \(k\) and \(p\) to be constant, the message size is thus linear in the security parameter \(\lambda\) with moderate cofactor.


