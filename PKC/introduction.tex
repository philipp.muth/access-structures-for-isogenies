\section{Introduction}

%\begin{comment}
% \subsection{Outline}
% \begin{itemize}
% 	\item \autoref{sec.prelim}: introduce secret sharing, HHS, and \textcolor{red}{compatiblity between them}
% 	\item \autoref{sec.kem}: What is a KEM, our scheme, introduce security notions for \(\sskem\), prove them
% 	\item \autoref{sec.signatures}: generalise signature algorithm, reprove security
% 	\item \autoref{sec.generalsss}: characterise necessary secret sharing properties, give working and non-working examples
% 	\item Conclusion: remaining issues for future work
% \end{itemize}
%\end{comment}

%\subsection{Motivation}
The principal motivation for a secret sharing scheme is to split private information into fragments and securely distribute these shares among a set of shareholders. Then, any collaborating set with a sufficient number of participants/shares is able to reconstruct the shared private information, while the secret remains confidential to any non-authorised, that is not sufficiently large, subset of shareholders.

Since their introduction in the 1970s by Blakley \cite{Blakley79} and Shamir \cite{Shamir79}, the field of secret sharing schemes, information theoretic and computational, has been studied extensively. %In previous years, however, interest in new developements and applications for secret sharing schemes seems to have declined.
In previous years, due to applications in blockchain and other scenarios, the interest in new developments and applications for secret sharing schemes has increased \cite{DBLP:conf/ccs/LindellN18, SP:DKLs19, DBLP:conf/sp/DoernerKLS18, Brandao_Davidson_Vassilev_2020}. 

Post-quantum schemes have, however, only received little attention with respect to secret sharing. Recently, De Feo and Meyer \cite{PKC:DeFMey20} proposed a key exchange mechanism and a signature scheme making use of isogeny based public key cryptography for which the secret key is stored in a Shamir shared way. Their approach enables decapsulation for the key exchange mechanism and signing for the signature scheme in a round-robin way without reconstructing the secret key in clear for any sufficiently large set of shareholders. Yet in applying Shamir's secret sharing scheme they restrict themselves to simple threshold access structures. Furthermore, their protocols are only passively secure, in that while a misbehaving shareholder cannot obtain information on the secret key shares of other shareholders participating in a decapsulation or a signing execution via maliciously formed inputs, his deviation from the protocol cannot be detected.

We aim to tackle both caveats by proposing an actively secure isogeny based key exchange mechanism, for which the secret key is secret shared by a trusted dealer. We further transform the key exchange mechanism into an actively secure signature scheme with shared secret key.\\

\noindent
\textbf{Our Contribution.}
Our contribution is manifold. First, we transfer the active security measures outlined in \cite{EPRINT:BDPV20} from their setting of full engagement protocols to a setting of threshold secret sharing. We thereby open the active security measures to a wider field of application and achieve significantly greater efficiency for said measures.
Second, we apply the adapted active security measures to propose an actively secure key exchange mechanism with secret shared secret key.
Third, we present an actively secure signature scheme by applying a Fiat-Shamir-transform to our key exchange mechanism.
And forth, we expand our key encapsulation mechanism and our signature scheme to application in general secret sharing schemes. For that we characterise the necessary properties for a secret sharing scheme and give several examples of compatible schemes. Hence we open those schemes to a significantly wider field of applications.\\

\begin{comment}
	\textbullet \- Characterisation of suitable secret sharing schemes for \cite{PKC:DeFMey20}

	\textbullet \- Transfer of algorithms to generalised setting

	\textbullet \- Security proof of emerging algorithms

	%\textbullet \- Fixing original proof gaps

	\textbullet \- Security model for secret shared key exchange mechanisms
\end{comment}

\noindent
\textbf{Related work.}
Secret sharing schemes were introduced by Blakley \cite{Blakley79} and Shamir \cite{Shamir79}. Shamir's scheme considers secret in \(\Z_p := \Z \bmod p\) with prime \(p\). To share \(s\in \Z_p\) among \(n\) sharholders, so that \(t\) or more shareholders are able to reconstruct it, a dealer samples a polynomial \(f \in \Z_p\left[ X\right]\) of degree \(t-1\) with constant term \(s\). The share of a shareholder with index \(i \in \left[ 1,\ldots, n\right]\) is \(f\paren* i\). To reconstruct a secret, a set of \(t\) or more shareholders interpolates the original polynomial \(f\) via Lagrange interpolation. Blakley takes a similar, yet distinct approach. 

Damg{\aa}rd et Thorbek \cite{PKC:DamTho06} presented a secret sharing scheme with secret space \(\Z\). Thorbek \cite{EPRINT:Thorbek09} later improved their scheme Yet their scheme is only computationally confidential, compared to the information theoretical confidentiality of Shamir and Blakley's schemes.
Tassa \cite{TCC:Tassa04} opened Shamir's scheme to more a general application by utilising the derivatives of the sharing polynomial to construct a hierarchical access structure,.
These basic secret sharing schemes rely on the dealer providing honestly generated shares to the shareholders. Verifiable secret sharing schemes eliminate this drawback by providing the shareholders with the means to verify the correctness of the received shares with varying overhead. Examples of these are \cite{CCS:BetKnoOtt93,AC:BacKatPat11,C:Pedersen91,EC:Stadler96}. With minor efficiency losses, Herranz et S{\'a}ez \cite{FC:HerSae03} were able to achieve verifiable secret sharing for generalised access structures. A more recent advancement was made in \cite{ICITS:TraDemBuc16}, in which the author presented a method to add and remove shareholders in  a verifiable secret sharing setting.

Traverso et al. \cite{AFRICACRYPT:TraDemBuc18} proposed an approach for evaluating arithmetic circuits on secret shared in Tassa's scheme, that also enabled auditing the results. 
%Tassa \cite{TCC:Tassa04}
%LISS \cite{PKC:DamTho06,EPRINT:Thorbek09}
%Giulia \cite{AFRICACRYPT:TraDemBuc18,ICITS:TraDemBuc16,EPRINT:TraDemBuc17,EPRINT:TDHB17}


Cozzo and Smart \cite{IMA:CozSma19} investigated the possibility of constructing shared secret schemes based on the Round 2 candidate signature schemes in the NIST standardization process\footnote{\url{https://csrc.nist.gov/Projects/post-quantum-cryptography/Post-Quantum-Cryptography-Standardization}}.
Based on CSI-FiSh \cite{AC:BeuKleVer19}, De Feo and Meyer \cite{PKC:DeFMey20} introduced threshold variants of passively secure encryption and signature schemes in the Hard Homogenous Spaces (HHS) setting. Cozzo and Smart \cite{PQCRYPTO:CozSma20} presented  the  first  actively  secure  but not robust distributed  signature  scheme based on isogeny assumptions. In \cite{EPRINT:BDPV20}, the authors presented CSI-RAShi, a robust and actively secure distributed key generation protocol based on Shamir's secret sharing in the setting of HHS, which necessitates all shareholders to participate.\\
%Since based on HHS, the shared secret in \cite{PKC:DeFMey20, PQCRYPTO:CozSma20} is reconstructed in a ring fashion, where participants have to compute their parts subsequently. 

\noindent
\textbf{Outline.}
In Section \ref{sec.prelim} the terminology, primitives and security notions relevant for this work are introduced.
Section \ref{sec.kem} presents an actively secure threshold key exchange mechanism and proves our scheme's active security and simulatability.
The actively secure signature scheme resulting from applying the Fiat-Shamir-transform to our key exchange mechanism is discussed in Section \ref{sec.signatures}.
%We will translate the results concerning active security from Section \ref{sec.kem} to a signature algorithm in Section \ref{sec. signatures}.
Finally, the necessary properties for a secret sharing scheme to be compatible with our key exchange mechanism and signature scheme are characterised in Section \ref{sec.generalsss} in order to enable applying a more general class of secret sharing schemes.


