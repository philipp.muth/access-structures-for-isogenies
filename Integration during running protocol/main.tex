\documentclass{scrartcl}

\input{preamble}

\title{Swapping parties during a protocol run}

\author{Philipp Muth}

%\institute{Technische Universit\"at Darmstadt, Security Engineering \and Univerist\"at Passau, Chair of Computer Engineering \and Technische Universit\"at Darmstadt, Cryptography and Computer Algebra}
\begin{document}
\maketitle

Let \(P := \set*{P_1, \ldots, P_n}\) be the set of all parties and \(\Gamma\) be an access structure. That is \(\Gamma \) contains all subsets of \(P\) that can reconstruct a shared secret. For a standard \(\left(k,n\right)\)-Shamir secret sharing scheme, this means \(\Gamma\) consists of all \(S\subset P\) with \(\#S \geq k\). \(\Gamma\) is monotonic, i.e., for all \(S\in\Gamma\) and \(S' \supset S\) we have \(S' \in \Gamma\).

Assume, that \(S\in\Gamma\) is a minimal, authorised set of parties, that has started executing an instance of Algorithm 1 in \cite{PKC:DeFMey20} and let \(S^\ast \subset S\) be the set of parties, that has already contributed. Hence \(S^\ast \neq \emptyset\). Now, let \(P_k\) be a party that wants to leave the execution but has not yet contributed. The reduced set of participants \(S\setminus\set*{P_k}\) is no longer authorised. Let therefore \(S'\) be an autorised extension of \(\left(S\setminus\set*{P_k}\right)\), i.e. \(S' \in \Gamma\) and \(S' \supset \left(S\setminus \set*{P_k}\right)\).

We will show, that \(S'\) cannot finish the execution of Algorithm 1 without each party in \(S^\ast\) having to send another message. That is \(S'\) cannot be more efficient than starting over.

%and let \(S'\neq S\) be another authorised set. For ease of argument, let \(\set*{P_k} = S \setminus S'\) and \(\set*{P_{k'}}= S' \setminus S\). Say, \(P_k\) wants to leave the execution of Algorithm 1 and is to be replaced be \(P_{k'}\) so that another authorised set \(S'\) can complete the execution. 

Denote the party, that contributes next after \(P_k\)'s announcing his dropping out, by \(P_{k'} \in S'\). \(P_{k'}\) receives the following from his predecessor:
\[ E^\ast := \left[ \sum_{P_i \in S^\ast} s_i L^S_{0,i} \right] E_0.\]
Without \(P_k\)'s dropping out, \(P_{k'}\)'s message would have been 
\[ \left[s_{k'} L^S_{0,k'}\right] E^\ast = \left[ \sum_{P_i \in S^\ast \cup \set*{P_{k'}}} s_i L^S_{0,i}\right] E_0,  \]
but since the executing set has changed, so have the reconstruction parameters \(L^\cdot_{0,i}\). Thus \(P_{k'}\) has to provide some \(L^\ast\) to compensate for the deprecated parameters of his predecessors. \(L^\ast\) has to fulfill
\begin{equation}
	\left[s_{k'} L^{S'}_{0,k'} L^\ast\right] \left(\left[\sum_{P_i \in S^\ast} s_i L^S_{0,i}\right] E_0\right) = \left[\sum_{P_i \in S^\ast \cup \set*{P_{k'}}} s_i L^{S'}_{0,i}\right] E_0,
\label{eq:1}\end{equation}
thus we have
\[L^\ast = \left(\sum_{P_i \in S^\ast} s_i L^{S'}_{0,i}\right) \cdot \left(\sum_{P_i \in S^\ast} s_i L_{0,i}^S\right)^{-1}.\]
\(L^\ast\) clearly is directly dependant on \(s_i\), for \(P_i \in S^\ast\). Hence, if \(P_{k'}\) can provide \(L^\ast\) that fulfills \eqref{eq:1}, he can determine the sum of the \(s_i\) with \(P_i \in S^\ast\). A contradiction.

\begin{comment}
We denote the output of Algorithm 1 by \(\left[s\right]E_0 = E\), where \(s\) is the shared secret. Since the shared secret \(s\) uniquely determined independent of the specific \(S\in\Gamma\) that executes Algorithm 1, \(E\) is also uniquely determined independent of \(S\). Now, assume that \(S\) have started an execution of Algorithm 1, but in turns\(P_k\)'s turn, he drops out before providing his input. By including \(P_{k'}\), \(S' := \left(S\setminus \set*{P_k}\right) \cup \set*{P_{k'}}\) (an authorised set) is constructed, hence a successful execution of Algorithm 1 is possible.

We will argue, that a party cannot be exchanged once the execution has been started. Hence, for a successful execution of Algorithm 1, \(S'\) has to start over. For that we assume, w.l.o.g., that \(P_{k}\) was the last party to provide its input and \(P_{k'}\) takes its place. \(P_{k'}\) receives 
\[E^\ast := \left[ \sum_{\substack{P_i \in S\\i\neq k'}}s_i L^S_{0,i}\right] E_0.\]
Due to changing from \(S\) to \(S'\), the parameters \(L^\cdot_{0,i}\) also have changed. Thus \(P_{k'}\) has to provide some \(L^\ast\) so that 
\[\left[s_{k'} L^\ast\right] \left(\left[\sum_{\substack{P_i \in S\\i\neq k'}}s_i L^S_{0,i}\right]E_0\right) = \left[s\right] E_0 = \left[s_{k'} L^{S'}_{0,k'}\right] \left(\left[\sum_{\substack{P_i \in S'\\i\neq k'}} s_i L^{S'}_{0,i}\right]E_0\right)\]
%\[\left[ \sum_{P_i \in S} s_i \cdot L^S_{0,i} \right] E_0 = \left[s\right] E_0 = \left[ \sum_{P_i \in S'} s_i \cdot L^{S'}_{0,i} \right] E_0.\]
Thus we have
\[L^\ast = L^{S'}_{0,k'} \left(\left[\sum_{\substack{P_i\in S'\\i\neq k'}} s_i L_{0,i}^{S'}\right]\right) \underbrace{\left(\sum_{\substack{P_i\in S'\\i\neq k'}} s_i L_{0,i}^{S}\right)^{-1},}_{\alpha :=}\]
where \(\alpha\) denotes the inverse of \(\left[sum_{\substack{P_i \in S'\\i\neq k'}} s_i L_{0,i}^S\right]\). We see, that \(L^\ast\) depends on \(s_i\) as well as \(L_{0,i}^S\) and \(L^{S'}_{0,i}\). The latter are publically known parameters, whereas if \(P_{k'}\) could determine \(s_i\), he could break the basic hardness assumption of the HHS. So \(P_{k'}\) cannot compute \(L^\ast\), so the parties in \(S'\) have to start over.

\end{comment}

\bibliographystyle{plain}
\bibliography{../cryptobib/abbrev0,../cryptobib/crypto,../cryptobib/my_crypto}


\end{document}

